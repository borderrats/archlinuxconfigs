![Border Rats](images/sittingduck003.png)





# Arch Linux Configurations #

 This repository contains markdown files and bash scripts used in configuring Arch Linux Workstations. 

Currently, this material represents a work in process. 

![Under Construction](images/underconstruction003.png)





You will find the documentation contains source and background articles, web sites and training videos. 

Before you use these setup and configuration instructions on your systems:

- Do the homework

- Read the Wiki's

- Watch the Videos

- Understand the meaning and impact of these configurations

- Knowledge is the power to innovate

  

## Working Hypothesis

With adequate technical knowledge and sensible maintenance procedures, Arch Linux systems can be configured as stable and efficient platforms for software development, containerization and virtual machine deployment.



## The Objective

Develop stable, low-maintenance configurations for high-end Arch Linux Workstations targeted at software development, containerization and virtual machine deployment.



## The Challenge - *It ain't easy!*

Arch Linux has a reputation for complex setup and configuration as well as operational instability. After all, with every installation, you are essentially building a new, customize operating system from the ground up. 

In addition, Arch Linux's architecture is based on the rolling release policy. Subject matter experts recommend a package update timetable ranging from every two days  to bi-weekly. Complexity plus frequent package updates constitute a risk of system failure. 



## The Opportunity - *Yes, but what if you could...*

- Complex installations mean that Arch Linux is highly customizable. This opens the possibility of designing an operating system which meets the special needs of high end software engineering workstations. These includes:

  - Maximizing efficiency by only installing needed packages, eliminating software bloat and reducing the risk of package conflicts.

  - Implementing File System Technologies like ***Btrfs*** and snapshots for solid data reliability and immediate system recovery.

  - Quickly adapting to changing environments by replacing or substituting new and improved software packages. 

    

- Rolling releases offer the opportunity to fix bugs and implement new features quickly thereby achieving productivity increases. Plus, rolling releases fit in well with the strategy of **Continuous Integration** and **Continuous Deployment**.

- Arch Linux offers a more open approach to package deployment, as compared to other Linux distributions, in that it readily accepts both free and paid software packages. In other words a "Whatever works" policy.



## Give Credit Where Credit is Due

A special thanks goes out to all those individuals and institutions cited as knowledge sources in the project documentation.

***"If I have seen further it is by standing on the shoulders of Giants."***

**Bernard of Chartres and Isaac Newton** 