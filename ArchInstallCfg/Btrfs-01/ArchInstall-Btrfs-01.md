[TOC]



# Arch Linux Installation - Btrfs 01



## Video Guide for Installation

This installation follows the pattern laid out in the video:

[Arch Linux Install: January 2021 ISO With BTRFS & Snapshots - YouTube](https://www.youtube.com/watch?v=Xynotc9BKe8&t=1005s)

As such this installation uses [Btrfs (archlinux.org)](https://wiki.archlinux.org/title/Btrfs) and [Snapper (archlinux.org)](https://wiki.archlinux.org/title/Snapper).

The assumption is that this configuration will result in snapshot backups which include the boot partition.



## Resources



### Official Arch Linux Sites

#### Btrfs

[Btrfs - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Btrfs)

[Using Btrfs with Multiple Devices - btrfs Wiki (kernel.org)](https://btrfs.wiki.kernel.org/index.php/Using_Btrfs_with_Multiple_Devices)

[btrfs Wiki (kernel.org)](https://btrfs.wiki.kernel.org/index.php/Main_Page)



#### Arch Linux Setup

[Arch-Setup-Script/install.sh at main · tommytran732/Arch-Setup-Script · GitHub](https://github.com/tommytran732/Arch-Setup-Script/blob/main/install.sh)



#### Downloads

[Arch Linux - Downloads](https://archlinux.org/download/)



#### Home

[Arch Linux](https://archlinux.org/)

[Table of contents - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Table_of_contents)



#### Installation Guide

[Installation guide - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Installation_guide)



#### Snapper

[Snapper - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Snapper)

[snapper-configs(5) — Arch manual pages (archlinux.org)](https://man.archlinux.org/man/snapper-configs.5)

[Snapper](http://snapper.io/) is a tool created by openSUSE's Arvin Schnell that helps with managing snapshots of [Btrfs](https://wiki.archlinux.org/title/Btrfs) subvolumes and thin-provisioned [LVM](https://wiki.archlinux.org/title/LVM) volumes. It can create and compare snapshots, revert between snapshots, and supports automatic snapshots timelines.



#### Wiki

[ArchWiki (archlinux.org)](https://wiki.archlinux.org/)



### Btrfs

[Btrfs - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Btrfs)

[Using Btrfs with Multiple Devices - btrfs Wiki (kernel.org)](https://btrfs.wiki.kernel.org/index.php/Using_Btrfs_with_Multiple_Devices)

[btrfs Wiki (kernel.org)](https://btrfs.wiki.kernel.org/index.php/Main_Page)





## Recover GRUB from the Arch ISO on a UEFI system

[Arch Linux Recovery: recover GRUB from the Arch ISO on a UEFI system - YouTube](https://www.youtube.com/watch?v=hwMa0PI6Aps)





## VMWare Configuration

### Download Arch Linux ISO

#### gpg - Verify ISO Signature

***gpg*** is already installed on Mike's Windows 10.

```bash
$ gpg --keyserver-options auto-key-retrieve --verify archlinux-version-x86_64.iso.sig
```



### Storage Configuration

**40-Gb Hard Drive**



### CPU Configuration

**2-CPU's 2-Threads each** 



### Memory Configuration

**5,120-Mb or 5-Gigs**



### Set UEFI Bios

**Virtual Machine Settings -> Options -> Advanced -> Firmware type**

```bash
Select UEFI button.
```



# Phase-1 Base Installation

This installation is based on the video:

[Arch Linux Install: January 2021 ISO With BTRFS & Snapshots - YouTube](https://www.youtube.com/watch?v=Xynotc9BKe8&t=1005s)

All Video time stamps relate to this video.



## Set the console keyboard layout

### Find Your Key Board

```bash
localectl list-keymaps | grep en | less

# Alternative Display Commands For Keymaps
# from Arch Linux Installation Guide
--------------------------------------------
ls /usr/share/kbd/keymaps/**/*.map.gz # List available keyboard layouts
ls /usr/share/kbd/keymaps/**/*.map.gz | less # To scroll keyboard layouts.

# Arch Linux Wiki
---------------------------------------------
localectl list-keymaps | grep -i search_term
```

The Default Keyboard is **US**.

**IMPORTANT:** Make a note of your keyboard you will need it later in the installation.

### Viewing keyboard settings

Use `localectl status` to view the current keyboard configurations.

### Mike's Default Keyboard

```bash
LANG=en_US.UTF-8
Believe the keymap is: "us"
/usr/share/kbd/keymaps/i386/qwerty/us.map.gz
```



### Configure Your Keyboard

The Default Keyboard is **US**.

```bash
loadkeys de_CH-latin1 # Load your key board
```



## Set Fonts - Optional

If necessary you can set the fonts to a larger size:

```bash
ter-132n
```



## Verify The Boot Mode

```bash
ls /sys/firmware/efi/efivars # From Arch Linux Installation Guide
# This should be UEFI or EFI

# If the command shows the directory without error, then the system is booted in UEFI mode. If the directory does not exist, the system may be booted in BIOS (or CSM) mode. If the system did not boot in the mode you desired, refer to your motherboard's manual.
```



## Connect to the Internet

### List Devices

#### Use *ip*

```bash
ip a # List Network Interface Devices
```

#### OR Use *ip link*

```bash
ip link <ENTER> # From Arch Linux Installation Guide
```

### Ping A Web Site

This will verify internet access.

```bash
 ping archlinux.org <ENTER> # Verify Internet Access
```

### My IP Address is

```bash
 192.168.0.7
```



## Update System Clock

```bash
timedatectl set-ntp true <ENTER> # Configure the System Clock
# Note: You are presented with some status output.
# In case screen Prompt will not display. Just press \<ENTER\> again.

timedatectl status <ENTER> # Verfiy System Clock status.
```





## Update Mirror List



### Create Your Mirror List

```bash
reflector -c 'United States' -a 6 --sort rate --save /etc/pacman.d/mirrorlist <ENTER>
# Copies Mirror List to the file: mirrorlist

pacman -Syy <ENTER> # Synchronize the server
```

If you encounter an error. Skip this step. Install reflector later before you reboot, and repeat this command.



## Partition the Disks

We have a ***UEFI*** installation, so we will be using ***gdisk*** and therefore we will NOT be using ***fdisk***.

### Lists Available Disks

```bash
lsblk <ENTER> # List all available disk drives.
# /dev/vda
# NOTE: Your Drive Name will vary!
# Mike's disk
# sda = /dev/sda
```

### Initiate Partition Operations

```bash
gdisk /dev/vda <ENTER> # 'vda' is the drive from 'lsblk'

# Mike's Disk
--------------------------------------------------------
gdisk /dev/sda <ENTER> # 'vda' is the drive from 'lsblk
```

### Partition #1- EFI Partition

```bash
n<ENTER>	# To create new partion number. You will then be asked
			# for the partition number.
1<ENTER>    # Default to 'one'	for the first partition
First sector	# Press<ENTER> - Take default.
Last sector: +500M<ENTER>	# +500M 500 Megabytes for partition #1 (Video said 300M)
Hex code or GUID: ef00 <ENTER> # 'e' 'f' zero zero = EFI Partition Type
```

### Partition #2- Swap Partition

```bash
n<ENTER>	# To create new partion number. You will then be asked
			# for the partition number.
2<ENTER>    # Default to 'two' for the second partition
First sector	# Press<ENTER> - Take default.
Last sector: +4G<ENTER>	# +4G = 4 Gigabytes for partition #2 (Video said 2GB)
Hex code or GUID: 8200 <ENTER> # SWAP Partition Type
```

### Partition #3- Main Data Partition

```bash
n<ENTER>	# To create new partion number. You will then be asked
			# for the partition number.
3<ENTER>    # Default to '3' for the third partition
First sector	# Press<ENTER> - Take default.
Last sector: <ENTER> # Take Default to assign remainder of disk to this partition.
Hex code or GUID: <ENTER> # Take default 8300 Linux Partition Type
```



### Write Changes to Disk

```bash
# Enter 'w'
w <ENTER>
# Now confirm by typing 'y'
y <ENTER>
```



### List the New Partitions

```bash
lsblk <ENTER> # List all available disk drives.
/dev/vda
  |
  ------- /dev/vda1 # EFI Partition
  ------- /dev/vda2 # SWAP Partition
  ------- /dev/vda3 # Main Data Partition
  
  # Your Drive Names will vary!
  # ---------------------------
  # Mike's Drives
  # ===========================
  /dev/sda1 # EFI Partition 500MB
  /dev/sda2 # Swap Partition 4Gb
  /dev/sda3 # Linux 35.5Gb
  
```



## Formatting Disks with File Systems

### Format EFI Partition: Fat-32

```bash
mkfs.fat -F32 /dev/vda1 # Format EFI Partition with Fat-32
# ========================================================
# Mike's Drive
mkfs.fat -F32 /dev/sda1

```



### Format Swap Partition

Video Time Stamp: 7:19

```bash
mkswap /dev/vda2
swapon /dev/vda2
# =====================================================
# Mike's Drive
mkswap /dev/sda2
swapon /dev/sda2
```



### Format Main Data Partition

Video Time Stamp: 7:37

```bash
mkfs.btrfs /dev/vda3
# ========================================================
# Mike's Drive
mkfs.btrfs /dev/sda3

```



## Create Mount Point for Main Data Partition

Video Time Stamp:  7:46

```bash
mount /dev/vda3 /mnt
# /mnt is the installation directory where we are going to install the system
# ==============================================
# Mike' Drive 
mount /dev/sda3 /mnt
```



## Create Subvolumes

Subvolume layout was taken from the Snapper recommendations found here: [Snapper - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Snapper)

### Create *root* Subvolume

Video Time Stamp: 8:24

```bash
btrsfs su cr /mnt/@ <ENTER> # su = subvolue cr = create
```

### Create *home* Subvolume

Video Time Stamp: 8:51

```bash
btrsfs su cr /mnt/@home <ENTER> # su = subvolue cr = create
```

### Create *snapshots* Subvolume

Video Time Stamp: 8:59

```bash
btrsfs su cr /mnt/@snapshots <ENTER> # su = subvolue cr = create
```

### Create *var_log* Subvolume

Video Time Stamp: 9:09

```bash
btrsfs su cr /mnt/@var_log <ENTER> # su = subvolue cr = create
```

## Unmount the */mnt* Directory

Video Time Stamp: 9:22

```bash
umount /mnt <ENTER>
```



## Mount Subvolumes

### Mount the *root* Subvolume

Video Time Stamp: 9:41

```bash
mount -o noatime,compress=lzo,space_cache=v2,subvol=@ /dev/vda3 /mnt <ENTER>
# Mounts the root directory to /mnt
# ===========================================================================
# Mike' Directory
mount -o noatime,compress=lzo,space_cache=v2,subvol=@ /dev/sda3 /mnt <ENTER>
```



### Create Directories for Remaining Subvolumes

Video Time Stamp: 12:24

```bash
mkdir -p /mnt/{boot,home,.snapshots,var/log} <ENTER>
# This will create
# /mnt/boot
# /mnt/home
# /mnt/.snapshots
# /mnt/var/log
# ======================
Note: The Vides uses var_log. Later instructions show var/log (separate directories) are a better way to go.

```

### Mount *Home* Subvolume

Video Time Stamp: 13:35

According to ***Btrfs*** wiki, once you use options on one subvolume they will be applied to all other subvolumes on that partition. Nevertheless, we will specify and apply the same options to all subvolumes.

```bash
mount -o noatime,compress=lzo,space_cache=v2,subvol=@home /dev/vda3 /mnt/home <ENTER>
# =============================================================
# Mike's Directories
mount -o noatime,compress=lzo,space_cache=v2,subvol=@home /dev/sda3 /mnt/home <ENTER>
```



### Mount *Snapshots* Subvolume

Video Time Stamp: 13:56

According to ***Btrfs*** wiki, once you use options on one subvolume they will be applied to all other subvolumes on that partition. Nevertheless, we will specify and apply the same options to all subvolumes.

```bash
mount -o noatime,compress=lzo,space_cache=v2,subvol=@snapshots /dev/vda3 /mnt/.snapshots <ENTER>
# ===============================================================
# Mike's Directories
mount -o noatime,compress=lzo,space_cache=v2,subvol=@snapshots /dev/sda3 /mnt/.snapshots <ENTER>


```



### Mount *var_log* Subvolume

Video Time Stamp: 14:21

```bash
mount -o noatime,compress=lzo,space_cache=v2,subvol=@var_log /dev/vda3 /mnt/var/log <ENTER>
# ===============================================================
# Mike's Directories
mount -o noatime,compress=lzo,space_cache=v2,subvol=@var_log /dev/sda3 /mnt/var/log <ENTER>

```



### Mount *boot* Subvolume

Video Time Stamp:

According to ***Btrfs*** wiki, once you use options on one subvolume they will be applied to all other subvolumes on that partition. Nevertheless, we will specify and apply the same options to all subvolumes.

```bash
mount /dev/vda1 /mnt/boot <ENTER>
# ===============================================================
# Mike's Directories
mount /dev/sda1 /mnt/boot <ENTER>
```



### List Partitions

```bash
lsblk <ENTER> # List all available disk drives.
# This will display the mount points on /dev/vda
```



## Install Base Packages

Video Time Stamp: 15:01

Use ***pacstrap*** to install base packages.

```bash
pacstrap /mnt base linux linux-firmware nano vim intel-ucode <ENTER>
# NOTE - Message:
# "error: command failed to execute correctly" is of no consequence. 
# Ignore this error messge.
```



## Generate File System Table

Video Time Stamp: 15:53

All the mount points are stored here.

### Transfer Mount Point Info to *fstab* File

```bash
genfstab -U /mnt  >> /mnt/etc/fstab # Transferring info to 'fstab' file.
```

### Review *fstab* File Contents

```bash
cat /mnt/etc/fstab <ENTER> 
# This will produce a listing of the 'fstab' file
# Check it out...
```



## Enter Installation Directory - *chroot*

Video Time Stamp: 16:34

Change root into the new system. [chroot - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Chroot)

```bash
arch-chroot /mnt <ENTER>
```



## Housekeeping



### Time Zones

Video Time Stamp: 16:48



#### Find Your Time Zone

```bash
timedatectl list-timezones | grep "Chicago"
# List all the time zones with 'Chicago' in the name.
# My Time Zone is:
America/Chicago
```



#### Create Link to Time Zone

[ln Command - IBM Documentation](https://www.ibm.com/docs/en/aix/7.2?topic=l-ln-command)

Create a symbolic link to ***zoneinfo***.

```bash
ln -sf /usr/share/zoneinfo/America/Chicago /etc/localtime <ENTER>
```



### Synchronize Hardware Clock and System Clock

Video Time Stamp: 17:43

```bash
hwclock --systohc <ENTER> # System clock and Hardware clock are now in sync.
```



### Configure File *locale.gen*

Video Time Stamp: 

#### Edit File *locale.gen*

Edit the ***locale.gen*** file and uncomment your locale (**en_US.UTF-8 UTF-8**)

```bash
nano /etc/locale.gen # Edit file locale.gen
# Uncomment line en_US.UTF-8 UTF.8
# Then save the file and exit
```



#### Generate Locale

```bash
locale-gen <ENTER> 
# This command will generate the 'en_US.UTF-8' locale
```



#### Transfer Locale Info to *locale.conf* File

Video Time Stamp: 18:27

```bash
nano /etc/locale.conf <ENTER> # Create and edit the 'locale.conf' file.
# On the first line of the empty file, add the line:
LANG=en_US.UTF-8

# Save the file and exit
```



### Add Your Keyboard to *vconsole.conf* File

Video Time Stamp: 18:45

```bash
nano /etc/vconsole.conf <ENTER> # Create and edit this file
# On the first line of the file, add the following:
KEYMAP=[YOUR KEYBOARD]
# =========================================
# Mike's Machine
KEYMAP=us

# Save and exit the file
```



### Create Machine Host Name

```bash
nano /etc/hostname <ENTER> # Create and Edit 'hostname' file

# On the first line of the new empty file, add the host name
# of your machine:
archbtrfs 

# =========================================================
# Mike's Machine
archbase01


# Save and exit the file
```



### Configure *host* File

Video Time Stamp: 19:28

```bash
nano /etc/hosts # Edit pre-existing 'host' file
# At the end of this file add the following:
127.0.0.1 <TAB> localhost
::1  <TAB><TAB> localhost
127.0.1.1 <TAB> archbase01.localdomain <TAB> archbase01

# NOTE: 'archbase01' is the Host Name of your machine
#        created above.

# Exit and save the file.
```



### Set *root* Password

Video Time Stamp 20:13

```bash
passwd<ENTER>
New password:<Enter your root password>
Retype new password: <Re-Enter root password>	
# ZRay972!3
# Enter the root password. 
# Don't lose it
```



## Install Needed Packages

Video Time Stamp: 20:23

```bash
pacman -S grub efibootmgr networkmanager network-manager-applet dialog wpa_supplicant mtools dosfstools git reflector snapper cups xdg-utils xdg-user-dirs alsa-utils pulseaudio inetutils base-devel linux-headers bash-completion plocate xclip unzip p7zip tree <ENTER>

# Proceed with installation
```



## Modify *mkinitcpio.conf* File

Video Time Stamp: 22:40

```bash
nano /etc/mkinitcpio.conf <ENTER> # Edit 'mkinitcpio.conf' file

# Modifiy the 'MODULES' statement as follows:
MODULES=(btrfs)

# Save and exit the file
```



## Recreate the Kernel Image with Btrfs

```bash
mkinitcpio -p linux <ENTER> # Generate the image
```



## Install *grub* Bootloader

Video Time Stamp: 22:32

```bash
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB <ENTER> # Install 'grub'
```



## Generate *grub* Configuration File

Video Time Stamp: 24:19

```bash
grub-mkconfig -o /boot/grub/grub.cfg <ENTER> # Parameter 'o' is o as in Oscar
```



## Enable Start Installed Services

Video Time Stamp: 24:42

### Enable *NetworkManager* Service

```bash
systemctl enable NetworkManager <ENTER>
```



### Enable *cups* Service

```bash
systemctl enable cups <ENTER>
```



## Create User for System

Video Time Stamp: 25:09

### Create User

```bash
useradd -mG wheel mike <ENTER>
# Adds user 'mike' to whell group
# -m Adds a home directory for 'mike'
# -G Giving the user the supplementry group 'wheel' which grants sudo
#    privleges.

```

### Create Password for New User

```bash

passwd mike <ENTER>
# Fill in the password for new user
New password:<Enter your user password>
Retype new password: <Re-Enter user password>	
# Don't lose password
XRay971!2


```



## Define *wheel* Group with *sudo* Privileges

### Edit the *visudo* File

```bash
EDITOR=nano visudo <ENTER>
```



### Modify *visudo* File

```bash
# Scroll down until you find the commented line:
# %wheel ALL=(ALL) ALL

# Uncomment this line:
%wheel ALL=(ALL) ALL

# Now all members of the 'wheel' Group will have 'sudo'
#   privileges.

# Save the file and exit!
```



## Exit Installation and Return to ISO

```bash
exit <ENTER>
# You may have to wait until jobs are finished
```



## Unmount Partitions

```bash
umount -a <ENTER>
# You will get some 'target is busy' messges.
# Ignore these messages and proceed to reboot.
```



## Reboot the Machine

```bash
reboot <ENTER>
```

If all goes well, the system will reboot and you will be greeted by the *grub* boot loader.

Select the Arch Linux option on the menu and continue.

**NOTE: If needed, Ctrl+X will reboot the system.**



## On System Reboot

### Log In on Reboot

Log in with your user name (NOT root).



### Check Network/Internet Connection

#### Use *ip* to List Devices

```bash
ip a # List Network Interface Devices
ip address = 192.168.0.7
```

#### OR Use *ip link*

```bash
ip link <ENTER> # From Arch Linux Installation Guide
```

#### Ping A Web Site

This will verify internet access.

```bash
 ping archlinux.org <ENTER> # Verify Internet Access
```



## Configure Snapper

Video Time Stamp: 28:16

The working assumption is that this configuration will allow the creation of snapshots which include the boot directory. The boot directory backups are triggered on kernel updates.

[System backup - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/System_backup#Snapshots_and_/boot_partition)



### Unmount Snapshots Directory

```bash
sudo umount /.snapshots <ENTER> # Unmount snapshots directory
```

### Remove Snapshots Directory

```bash
sudo rm -r /.snapshots <ENTER>
```

### Create Snapper Configuration

```bash
sudo snapper -c root create-config / <ENTER> 
# 'root' is an arbitrary name. Use any name you choose.
```

### Delete Snapshots Subvolume

***Snapshot*** created an unwanted subvolume. We need to delete it.

```bash
sudo btrfs subvolume delete /.snapshots <ENTER>
# We already created a subvolume and don't need this one.
```

### Recreate the Snapshots Directory

```bash
sudo mkdir /.snapshots <ENTER>
```

### Remount Snapshots Subvolume

```bash
sudo mount -a <ENTER>
```

### Set Permissions on Snapshot Directory

```bash
sudo chmod 750 /.snapshots <ENTER> 
# Snaper snapshots will now be placed outside the root subvolume so
# that the root subvolume can easily be replaced with a snapshot
# version.
```

### Modify Snapper Configuration File

#### Edit Snapper Configuration File

```bash
sudo nano /etc/snapper/configs/root <ENTER>
```

#### Modify Allow Users Parameter

```bash
# Find the line
#  ALLOW_USERS=""

# Replace it with the line
ALLOW_USERS="mike" # Add your user name
# This will allow you access to snapshots.

# DO NOT EXIT THE EDIT SESSION YET
# THERE IS MORE WORK TO DO
```

#### Modify Timeline Cleanup

[Snapper - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Snapper#Automatic_timeline_snapshots)

[snapper-configs(5) — Arch manual pages (archlinux.org)](https://man.archlinux.org/man/snapper-configs.5)

```bash
# Scroll down and find the 'timeline cleanup' block
# shown below:

TIMELINE_MIN_AGE="1800"
TIMELINE_LIMIT_HOURLY="10"
TIMELINE_LIMIT_DAILY="10"
TIMELINE_LIMIT_WEEKLY="0"
TIMELINE_LIMIT_MONTHLY="10"
TIMELINE_LIMIT_YEARLY="10"

# Modify these parameters as follows:
TIMELINE_MIN_AGE="1800"
TIMELINE_LIMIT_HOURLY="5"
TIMELINE_LIMIT_DAILY="7"
TIMELINE_LIMIT_WEEKLY="0"
TIMELINE_LIMIT_MONTHLY="0"
TIMELINE_LIMIT_YEARLY="0"

```



#### Save & Exit Snapshot Configuration File

```bash
# Save and Exit /etc/snapper/configs/root
```



#### Enable Snapper Timeline

```bash
sudo systemctl enable --now snapper-timeline.timer <ENTER>
```



#### Enable Snapper Cleanup

```bash
sudo systemctl enable --now snapper-cleanup.timer <ENTER>
```



## Install Arch User Repository Packages

### Install *yay*

#### Clone *yay* Repository

```bash
git clone https://aur.archlinux.org/yay <ENTER>
# Install 'yay'
```

#### Move to *yay* Directory

```bash
cd yay/ # Move to 'yay' directory
```

#### Build *yay* Package

```bash
makepkg -si PKGBUILD <ENTER> # Build the 'yay' package
```

### Return to Home Directory

```bash
cd # Return to Home Directory
```



### Install Snapper AUR Packages

```bash
yay -S snap-pac-grub snapper-gui <ENTER>
# Install:
#  snap-pac-grub
#  snapper-gui
Differences to show. # Take option 'None'
# You may be asked to install a 'key'. Take the default and press ENTER.

```

Note: **snap-pac-grub** includes ***snap-pac***.

[AUR (en) - snap-pac-git (archlinux.org)](https://aur.archlinux.org/packages/snap-pac-git/)

[GitHub - wesbarnett/snap-pac: Pacman hooks that use snapper to create pre/post btrfs snapshots like openSUSE's YaST](https://github.com/wesbarnett/snap-pac)

[snap-pac — snap-pac documentation (wesbarnett.github.io)](https://wesbarnett.github.io/snap-pac/)

After this installation, you should reports of actual snapshots taken before and after installing new packages.

[AUR (en) - snap-pac-grub (archlinux.org)](https://aur.archlinux.org/packages/snap-pac-grub/)

[AUR (en) - snapper-gui-git (archlinux.org)](https://aur.archlinux.org/packages/snapper-gui-git/)



### Install Graphics Card

If you are installing on bare metal, you will need to install drivers for a graphics card. If you are installing on a Virtual Machine, skip this step.

#### Background

[AMDGPU - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/AMDGPU#Selecting_the_right_driver)

[Xorg - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Xorg#Driver_installation)

#### AMD Video Cards

[Arch Linux - xf86-video-amdgpu 21.0.0-2 (x86_64)](https://archlinux.org/packages/extra/x86_64/xf86-video-amdgpu/)

#### Intel Video Cards

[Arch Linux - xf86-video-intel 1:2.99.917+916+g31486f40-2 (x86_64)](https://archlinux.org/packages/extra/x86_64/xf86-video-intel/)

#### New Nvidia Cards

[Arch Linux - nvidia 495.46-9 (x86_64)](https://archlinux.org/packages/extra/x86_64/nvidia/)

[Arch Linux - nvidia-utils 495.46-2 (x86_64)](https://archlinux.org/packages/extra/x86_64/nvidia-utils/)



```bash
pacman -S xf86-video-amdgpu
pacman -S xf86-video-intel
pacman -S nvidia nvidia-utils
```



# Phase-2 Install Desktop Environment



[How to Properly Install KDE on Arch Linux Step by Step -12-14-2021 (itsfoss.com)](https://itsfoss.com/install-kde-arch-linux/)

### Install Plasma Packages

```bash
sudo pacman -S xorg plasma plasma-wayland-session kde-applications firefox <ENTER>
# Command contains all the necessary packages
```



### Enable *sddm*

Simple Desktop Display Manager

#### Resources

[SDDM - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/SDDM)

[GitHub - sddm/sddm: QML based X11 and Wayland display manager](https://github.com/sddm/sddm/)

[What is Display Manager in Linux? (itsfoss.com)](https://itsfoss.com/display-manager/)

```bash
systemctl enable sddm.service <ENTER>
```



## Configure Display Settings

### Display Resolution

1920x1080 (16:9)

1440x900 (16:10)

1280x720 (16:9) *

1024x768 (4:3)

\*Current Resolution





## Configure Boot Directory Snapshots

```bash
sudo mkdir /etc/pacman.d/hooks <ENTER>

sudo nano /etc/pacman.d/hooks/50-bootbackup.hook <ENTER>
```

### *50-bootbackup.hook*

```bash
[Trigger]
Operation = Upgrade
Operation = Install
Operation = Remove
Type = Path
Target = boot/*

[Action]
Depends = rsync
Description = Backing up /boot...
When = PreTransaction
Exec = /usr/bin/rsync -a --delete /boot /.bootbackup
# Save and Exit the file
```



### Install *rsync*

```
sudo pacman -S rsync
```



### Change Ownership on Snapshots

```bash
sudo chmod a+rx /.snapshots <ENTER>

sudo chown :mike /.snapshots <ENTER>
```



### Run Snapper GUI from Terminal

```bash
snapper-gui <ENTER>
```



### Shutdown

```bash
shutdown now <ENTER> # Shutdown and Power Off
```

### Take a VM Snapshot

Take a VM Snapshot at this point. This snapshot will contain the base Arch Linux Installation.

### Power-On

Re-Start your system and sign on to your user account (***mike***).

Configure KDE-Plasma Desktop Environment.

[Detailed Guide to Configure KDE Plasma Desktop - Technastic](https://technastic.com/configure-kde-plasma-desktop/)



# Phase-3 Configure Applications



## Install *open-vm-tools*

### Copy and Paste WILL NOT WORK on Wayland

**COPY AND PASTE WILL NOT CURRENTLY WORK WITH WAYLAND!!!**

Currently, VMware Copy and Paste operations will not work if the desktop is running Wayland. However, it is possible to configure a shared directory whereby the Windows host and the Arch Linux guest can share files. See the section labeled **Shared Folder**, below.

[VMware/Install Arch Linux as a guest - ArchWiki](https://wiki.archlinux.org/title/VMware/Install_Arch_Linux_as_a_guest#VMware_Tools_versus_Open-VM-Tools)

```bash
sudo pacman -S gtkmm <ENTER>
sudo pacman -S gtkmm3 <ENTER>
sudo pacman -S gtkmm4 <ENTER>
sudo pacman -S open-vm-tools <ENTER>
sudo pacman -S xf86-video-vmware xf86-input-vmmouse <ENTER>
sudo systemctl enable vmtoolsd <ENTER>
reboot <ENTER>

```

### Installation Video

[How to Install VMware Tools (Open VM Tools) in Arch Linux 2020.11 - YouTube](https://www.youtube.com/watch?v=KegKUyhgSFI)

### VMware Tools

[Arch Linux - gtkmm 1:2.24.5-4 (x86_64)](https://archlinux.org/packages/extra/x86_64/gtkmm/)

[Arch Linux - open-vm-tools 6:11.3.5-1 (x86_64)](https://archlinux.org/packages/community/x86_64/open-vm-tools/)

[GitHub - vmware/open-vm-tools: Official repository of VMware open-vm-tools project](https://github.com/vmware/open-vm-tools)

[Install Official VMware Tools on Arch Linux. | by Ribo Mo | Medium](https://medium.com/@ribomo42/install-official-vmware-tools-on-arch-linux-15afd5b848ed)

### Video and Mouse Drivers

[Arch Linux - xf86-video-vmware 13.3.0-3 (x86_64)](https://archlinux.org/packages/extra/x86_64/xf86-video-vmware/)

[Arch Linux - xf86-input-vmmouse 13.1.0-6 (x86_64)](https://archlinux.org/packages/extra/x86_64/xf86-input-vmmouse/)

### Shared Folder

##### Resources

[VMware/Install Arch Linux as a guest - ArchWiki](https://wiki.archlinux.org/title/VMware/Install_Arch_Linux_as_a_guest#Shared_Folders_with_vmhgfs-fuse_utility)

##### VM Host Shared Folder

```bash
D:\VirtualMachinesShared\data
```



##### Connection Commands

**This works!**

```bash
vmware-hgfsclient # Displays name of Shared Device/Volume

sudo mkdir /mnt/vmShare <ENTER>

vmhgfs-fuse -o allow_other -o auto_unmount .host:/SharedData /mnt/vmShare
```

The ***vmhgfs-fuse*** command must be run manually before access to the vm share is configured.

Clones man configure shared data for automatic configuration at ***boot***.

See: [VMware/Install Arch Linux as a guest - ArchWiki](https://wiki.archlinux.org/title/VMware/Install_Arch_Linux_as_a_guest#Enable_at_boot)



## Install *mlocate* (a.k.a. *locate*)

```bash
sudo pacman -S mlocate <ENTER>

sudo updatedb <ENTER>
```

After installation use ***locate*** to search for files.





## Install *dust*

Disk usage utility.



[Arch Linux - dust 0.7.5-1 (x86_64)](https://archlinux.org/packages/community/x86_64/dust/)

[GitHub - bootandy/dust: A more intuitive version of du in rust](https://github.com/bootandy/dust)

[Replace du with dust on Linux | Opensource.com](https://opensource.com/article/21/6/dust-linux)

[Switching To The Alacritty Terminal Emulator - YouTube](https://www.youtube.com/watch?v=PZPMvTvUf1Y) 12:43

```bash
sudo pacman -S dust
```



# Error Management

[root device not read-write warning / [testing\] Repo Forum / Arch Linux Forums](https://bbs.archlinux.org/viewtopic.php?id=167153)

[Snapper won't restore : archlinux (reddit.com)](https://www.reddit.com/r/archlinux/comments/qyc1dm/snapper_wont_restore/)

[Snapper - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Snapper#Restoring_/_to_its_previous_snapshot)

[Snapper/BTRFS layout for easily restoring files, or entire system / Installation / Arch Linux Forums](https://bbs.archlinux.org/viewtopic.php?id=194491#:~:text=If your system dies however%2C and you need,snapshot you want to restore the system to%3A)





## Fix "invalid or corrupted package (PGP signature)" error in Arch Linux

### Repair Corrupted Files

https://ostechnix.com/fix-invalid-corrupted-package-pgp-signature-error-arch-linux/

To solve "invalid or corrupted package (PGP signature)" error in Arch Linux, we need to update the `archlinux-keyring` package.

To do so, run:

```
$ sudo pacman -S archlinux-keyring
```

**Sample output:**

```
resolving dependencies...
looking for conflicting packages...

Packages (1) archlinux-keyring-20170104-1

Total Installed Size: 0.81 MiB
Net Upgrade Size: 0.04 MiB

:: Proceed with installation? [Y/n] y
(1/1) checking keys in keyring [######################] 100%
(1/1) checking package integrity [######################] 100%
(1/1) loading package files [######################] 100%
(1/1) checking for file conflicts [######################] 100%
(1/1) checking available disk space [######################] 100%
:: Processing package changes...
(1/1) upgrading archlinux-keyring [######################] 100%
==> Appending keys from archlinux.gpg...
gpg: marginals needed: 3 completes needed: 1 trust model: PGP
gpg: depth: 0 valid: 1 signed: 6 trust: 0-, 0q, 0n, 0m, 0f, 1u
gpg: depth: 1 valid: 6 signed: 69 trust: 0-, 0q, 0n, 6m, 0f, 0u
gpg: depth: 2 valid: 69 signed: 7 trust: 69-, 0q, 0n, 0m, 0f, 0u
gpg: next trustdb check due at 2017-09-07
==> Locally signing trusted keys in keyring...
 -> Locally signing key 0E8B644079F599DFC1DDC3973348882F6AC6A4C2...
 -> Locally signing key 684148BB25B49E986A4944C55184252D824B18E8...
 -> Locally signing key 91FFE0700E80619CEB73235CA88E23E377514E00...
 -> Locally signing key 44D4A033AC140143927397D47EFD567D4C7EA887...
 -> Locally signing key 27FFC4769E19F096D41D9265A04F9397CDFD6BB0...
 -> Locally signing key AB19265E5D7D20687D303246BA1DFB64FFF979E7...
==> Importing owner trust values...
==> Disabling revoked keys in keyring...
 -> Disabling key F5A361A3A13554B85E57DDDAAF7EF7873CFD4BB6...
 -> Disabling key 7FA647CD89891DEDC060287BB9113D1ED21E1A55...
 -> Disabling key D4DE5ABDE2A7287644EAC7E36D1A9E70E19DAA50...
 -> Disabling key BC1FBE4D2826A0B51E47ED62E2539214C6C11350...
 -> Disabling key 9515D8A8EAB88E49BB65EDBCE6B456CAF15447D5...
 -> Disabling key 4A8B17E20B88ACA61860009B5CED81B7C2E5C0D2...
 -> Disabling key 63F395DE2D6398BBE458F281F2DBB4931985A992...
 -> Disabling key 0B20CA1931F5DA3A70D0F8D2EA6836E1AB441196...
 -> Disabling key 8F76BEEA0289F9E1D3E229C05F946DED983D4366...
 -> Disabling key 66BD74A036D522F51DD70A3C7F2A16726521E06D...
 -> Disabling key 81D7F8241DB38BC759C80FCE3A726C6170E80477...
 -> Disabling key E7210A59715F6940CF9A4E36A001876699AD6E84...
==> Updating trust database...
gpg: next trustdb check due at 2017-09-07
```

The above command will update the new keys and disable the revoked keys in your Arch Linux system.

Again, I tried to upgrade my Arch Linux using command:

```
$ sudo pacman -Syu
```

### Restoring Snapper Snapshot

[Snapper - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Snapper#Restoring_/_to_its_previous_snapshot)
