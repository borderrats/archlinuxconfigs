[TOC]



# Restoring System Backup

- Procedure - Restore as Default Boot Version

  #### 1. Configure Arch Linux ISO on VMware CD ROM Drive

  Make sure the the ARCH Linux ISO is available on the CD ROM Drive.

  #### 2. Identify Snapshot to restore

  ##### 	Command Line

  ​	[Snapper - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Snapper#Restoring_/_to_its_previous_snapshot)

  ##### 	Snapper GUI

  Boot to your current system GUI. Use the ***Snapper GUI***  application to identify the number of the snapshot you want to restore. 

  #### 2. Boot The System from Ach Linux ISO

  - Boot from Arch Linux ISO
  - Select UEFI Boot Manager and enter this application
  - Select the Arch Linux ISO on the CD ROM drive and boot to it.
    - You will need to select the CD ROM from the options list and Click it. 
    - This will cause the system to reboot from the CD ROM using the Arch Linux ISO

  #### 3. Restore Target Snapshot

  Reference: [Snapper - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Snapper#Restoring_/_to_its_previous_snapshot)	Restoring a snapshot

  ```bash
  $ lsblk <ENTER> # Review the connected devices.
  ```

  

  1. Mount ***/dev/sda3*** 

     ```bash
     mount /dev/sda3 /mnt # My installation directory is on 'sda3'
     ```

  2. Look up the snapshot to be restored:

     ```bash
     nano /mnt/@snapshots/*/info.xml # Scroll through files hitting Alt+Some Key I don't recognize.
     ```

  3. Remove the old version:

     ```bash
     rm -rf /mnt/@ <ENTER> # This might take a while.
     ```

  4. Restore the target snapshot. 

     ```bash
     btrfs subvol snapshot /mnt/@snapshots/2/snapshot /mnt/@ <ENTER>
     ```

     

  5. Change subvolid in the `/` mount entry option in [fstab](https://wiki.archlinux.org/title/Fstab) to the new subvolid.

  [Snapper - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Snapper#Restoring_/_to_its_previous_snapshot)

  The video [How to install Arch Linux with BTRFS & Snapper - YouTube](https://www.youtube.com/watch?v=sm_fuBeaOqE&t=5s) says this is not necessary. [Snapper - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Snapper#Restoring_/_to_its_previous_snapshot) says it is.

  

  Change subvolid in the `/` mount entry option in [fstab](https://wiki.archlinux.org/title/Fstab) to the new subvolid.

  ```bash
  # btrfs subvolume list /mnt | grep @$ # Check the new subvolid
  # My new subvolid is 301
  # -------------------------------------------------------------
  # grep "/ " /mnt/@/etc/fstab          # Check the old subvolid
  # My old subvolid is 256
  # vim /mnt/@/etc/fstab                # Edit
  # Change old subvolid 256 to new subvolid 301
  
  ```

  Your `/` has now been restored to the previous snapshot. Now just simply reboot.

  