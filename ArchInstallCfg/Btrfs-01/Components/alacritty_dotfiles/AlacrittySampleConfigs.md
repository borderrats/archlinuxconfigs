[TOC]



# Alacritty Sample Configuration



## Shell Configuration



```
# Shell
#
# You can set shell.program to the path of your favorite shell, e.g. /bin/zsh.
# Entries in shell.args are passed unmodified as arguments to the shell.
#
# shell:
#   program: /usr/local/bin/fish
#   args:
#     - --login
#
shell:
  program: /usr/local/bin/tmux
  args:
    - new-session
    - -A
    - -D
    - -s
    - main

```

