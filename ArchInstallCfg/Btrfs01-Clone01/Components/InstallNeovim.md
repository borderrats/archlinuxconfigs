

[TOC]



# Arch Linux Neovim Installation

[Home - Neovim](https://neovim.io/)

## Resources



### Videos

[Vim vs NeoVim, What's the Difference? Which Should You Use? - YouTube](https://www.youtube.com/watch?v=R8tI4gpzkE4)

#### ChrisAtMachine

[Neovim - Why I'm switching to Native LSP over CoC - YouTube](https://www.youtube.com/watch?v=190HoB0pVro)

[ChrisAtMachine - Neovim Training - YouTube](https://www.youtube.com/channel/UCS97tchJDq17Qms3cux8wcA)

[ChrisAtMachine - Navigating your files with Neovim and Ranger - YouTube](https://www.youtube.com/watch?v=8qfc7Nqd5hw)

[Chris At Machine - FZF will change your workflow completely - YouTube](https://www.youtube.com/watch?v=on1AzaZzQ7k&list=RDCMUCS97tchJDq17Qms3cux8wcA&index=2)

#### NuralNine

[NuralNine - Awesome Neovim Setup From Scratch - Full Guide - YouTube](https://www.youtube.com/watch?v=JWReY93Vl6g)

[NuralNine - Switching to Neovim & Rebuilding my Configuration - YouTube](https://www.youtube.com/watch?v=Kx-SDJwL01o)

### Articles

[ChrisAtMachine - Integrate Neovim with FZF & more |(chrisatmachine.com)](https://www.chrisatmachine.com/Neovim/08-fzf/)

### Github

[GitHub - ChristianChiarulli/nvim: My neovim config](https://github.com/ChristianChiarulli/nvim)

### Website

[Home - Neovim](https://neovim.io/)



### Plugins

#### Code Completion

##### coc.nvim

[GitHub - neoclide/coc.nvim: Nodejs extension host for vim & neovim, load extensions like VSCode and host language servers.](https://github.com/neoclide/coc.nvim)

[Create coc.nvim extension to improve vim experience | by chemzqm | Medium](https://medium.com/@chemzqm/create-coc-nvim-extension-to-improve-vim-experience-4461df269173)



#### Golang - Plugin

[GitHub - fatih/vim-go: Go development plugin for Vim](https://github.com/fatih/vim-go)



#### junegunn - Plugins

[GitHub - junegunn/vim-plug: Minimalist Vim Plugin Manager](https://github.com/junegunn/vim-plug)



## Color Schemes

[GitHub - folke/tokyonight.nvim: 🏙 A clean, dark Neovim theme written in Lua, with support for lsp, treesitter and lots of plugins. Includes additional themes for Kitty, Alacritty, iTerm and Fish.](https://github.com/folke/tokyonight.nvim)

[GitHub - LunarVim/onedarker.nvim: Onedark inspired colorscheme written in lua.](https://github.com/LunarVim/onedarker.nvim)

[GitHub - LunarVim/darkplus.nvim](https://github.com/LunarVim/darkplus.nvim)

[GitHub - LunarVim/Colorschemes: Collection of colorschemes made to be compatible with LunarVim](https://github.com/LunarVim/Colorschemes)



# Neovim Installation

## AppImage

Install Neovim from ***AppImage***

https://www.youtube.com/watch?v=rHxgQ8c6H7k

Download AppImage

Copy to Scratch Directory

Rename nvim.appimage  to nvim

Change Ownership to root

```
chmod +x nivm
sudo chown root:root nvim
mv nvim /usr/bin

```



# Lua

[Lua: getting started](https://www.lua.org/start.html)

[ZeroBrane Studio - Lua IDE/editor/debugger for Windows, Mac OSX, and Linux](https://studio.zerobrane.com/)

[LuaRocks - The Lua package manager](https://luarocks.org/)

## LSP Server

[GitHub - williamboman/nvim-lsp-installer: Companion plugin for nvim-lspconfig that allows you to seamlessly manage LSP servers locally with :LspInstall. With full Windows support!](https://github.com/williamboman/nvim-lsp-installer)

[GitHub - neovim/nvim-lspconfig: Quickstart configurations for the Nvim LSP client](https://github.com/neovim/nvim-lspconfig)



# ChrisAtMachine Training

### Videos

#### Neovim Setup Series

[Neovim IDE from Scratch - Introduction (100% lua config) - YouTube](https://www.youtube.com/watch?v=ctH-a-1eUME&list=PLhoH5vyxr6Qq41NFL4GvhFp-WLd5xzIzZ)



### ChrisAtMachine Resources

#### Github

[GitHub - LunarVim/Neovim-from-scratch: A Neovim config designed from scratch to be understandable](https://github.com/LunarVim/Neovim-from-scratch)

####  Blog

[Home | Chris@Machine (chrisatmachine.com)](https://www.chrisatmachine.com/)



### *packer.nvim*

Plugin manager for ***neovim*** written in ***lua***.

[GitHub - wbthomason/packer.nvim: A use-package inspired plugin manager for Neovim. Uses native packages, supports Luarocks dependencies, written in Lua, allows for expressive config](https://github.com/wbthomason/packer.nvim)

### Completion

#### *nvim-cmp* Plugin

[GitHub - hrsh7th/nvim-cmp: A completion plugin for neovim coded in Lua.](https://github.com/hrsh7th/nvim-cmp)

#### Lua Snippets

[GitHub - L3MON4D3/LuaSnip: Snippet Engine for Neovim written in Lua.](https://github.com/L3MON4D3/LuaSnip)

[GitHub - rafamadriz/friendly-snippets: Set of preconfigured snippets for different languages.](https://github.com/rafamadriz/friendly-snippets)

#### Nerd Fonts

[Nerd Fonts - Iconic font aggregator, glyphs/icons collection, & fonts patcher](https://www.nerdfonts.com/cheat-sheet)



## Neovide

A graphical user interface for Neovim.

[Neovide Is A Graphical Neovim Client Written In Rust - YouTube](https://www.youtube.com/watch?v=Vd5AACp6GG0)

[GitHub - neovide/neovide: No Nonsense Neovim Client in Rust](https://github.com/neovide/neovide)



# Neovim Installation Procedure - Arch Linux



## Install Neovim

[Installing Neovim · neovim/neovim Wiki · GitHub](https://github.com/neovim/neovim/wiki/Installing-Neovim)

[Arch Linux - neovim 0.6.1-1 (x86_64)](https://archlinux.org/packages/community/x86_64/neovim/)

```bash
sudo pacman -S neovim <ENTER> 
```

## Python



### Install Python Client for Neovim

Pynvim implements support for python plugins in Nvim. It also works as a library for connecting to and scripting Nvim processes through its msgpack-rpc API.

[GitHub - neovim/pynvim: Python client and plugin host for Nvim](https://github.com/neovim/pynvim)

[Arch Linux - python-pynvim 0.4.3-2 (any)](https://archlinux.org/packages/community/any/python-pynvim/)

```bash
sudo pacman -S python-pynvim <ENTER> 
```



### Install Python2 for Neovim

[Arch Linux - python2 2.7.18-5 (x86_64)](https://archlinux.org/packages/extra/x86_64/python2/)

```bash
sudo pacman -S python2 <ENTER>
```

### Add Python Packages to *$PATH*

List and identify the installed python packages which will be added to your path:

```bash
ls /usr/bin/python*
```



Use ***nano*** to edit and add the following directories to your $PATH in ~/.profile:

```bash
/usr/bin/python2            
/usr/bin/python2.7    
/usr/bin/python3
/usr/bin/python3-futurize
/usr/bin/python3-pasteurize
/usr/bin/python3-config    
/usr/bin/python3.8 
/usr/bin/python3.8-config
```

Example:

```bash
PATH="$PATH:/usr/bin/python2:/usr/bin/python2.7:/usr/bin/python3:/usr/bin/python3-futurize:/usr/bin/python3-pasteurize:/usr/bin/python3-config:/usr/bin/python3.8:/usr/bin/python3.8-config"
```



Add the items to your path immediately:

Add the "PATH" to your ***~/.profile*** file. Then activate the "PATH" with the following command.

```bash
source ~/.profile
```



Verify the path:

```bash
echo $PATH
```



### Install Node.js 

***node*** and ***npm*** are required. 

#### *npm*

[npm (npmjs.com)](https://www.npmjs.com/)

[npm · GitHub](https://github.com/npm)

[Node.js - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Node.js)

[Arch Linux - npm 8.4.1-1 (any)](https://archlinux.org/packages/community/any/npm/)

[Downloading and installing Node.js and npm | npm Docs (npmjs.com)](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

Use npm to install ***Node.js***.

```bash
sudo pacman -S npm <ENTER>
```

### Check Installed Versions

```bash
node -v
npm -v
```

### Get Latest *npm* Release

**Note:** to download the latest version of npm, on the command line, run the following command:

```
npm install -g npm
```

#### *node*

##### *Node.js* Background

[Node.js (nodejs.org)](https://nodejs.org/en/)

[Node.js - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Node.js)

##### *npm* Documentation for Linux

[GitHub - npm/documentation: Documentation for the npm registry, website, and command-line interface.](https://github.com/npm/documentation)

[npm · GitHub](https://github.com/npm)

##### Node Installation on Linux

[GitHub - nvm-sh/nvm: Node Version Manager - POSIX-compliant bash script to manage multiple active node.js versions](https://github.com/nvm-sh/nvm)

Install ***Node.js*** using ***npm***.

Example Instllations:

```bash
$ nvm use 16
Now using node v16.9.1 (npm v7.21.1)
$ node -v
v16.9.1
$ nvm use 14
Now using node v14.18.0 (npm v6.14.15)
$ node -v
v14.18.0
$ nvm install 12
Now using node v12.22.6 (npm v6.14.5)
$ node -v
v12.22.6
```



### Install *ruby* and *gem*

#### *ruby* Background

[Ruby - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Ruby)

[GitHub - rbenv/rbenv: Manage your app's Ruby environment](https://github.com/rbenv/rbenv)

#### *rbenv*

Install ***ruby*** using AUR package ***rbenv***

[GitHub - rbenv/rbenv: Manage your app's Ruby environment](https://github.com/rbenv/rbenv)



##### Install *rbenv*

[AUR (en) - rbenv (archlinux.org)](https://aur.archlinux.org/packages/rbenv)

```bash
Badderman commented on 2021-02-13 11:28 (edited on 2021-02-13 12:49 by Badderman)
Run "rbenv rehash" hooks automatically after "gem install/uninstall":
```

[GitHub - rbenv/rbenv: Manage your app's Ruby environment](https://github.com/rbenv/rbenv)

```bash
yay -S rbenv # Install with 'yay'
```

```bash
Your shell must be initialized before rbenv will function correctly.
Run the following, and consider adding it to your ~/.bashrc:
eval "$(rbenv init -)"

Optional dependencies for rbenv:
  ruby-build
```



### How rbenv hooks into your shell

#### *rbenv* Post-Install Background

[GitHub - rbenv/rbenv: Manage your app's Ruby environment](https://github.com/rbenv/rbenv#how-rbenv-hooks-into-your-shell)

Skip this section unless you must know what every line in your shell profile is doing.

`rbenv init` is the only command that crosses the line of loading extra commands into your shell. Coming from RVM, some of you might be opposed to this idea. Here's what `rbenv init` actually does:

1. Sets up your shims path. This is the only requirement for rbenv to function properly. You can do this by hand by prepending `~/.rbenv/shims` to your `$PATH`.
2. Installs autocompletion. This is entirely optional but pretty useful. Sourcing `~/.rbenv/completions/rbenv.bash` will set that up. There is also a `~/.rbenv/completions/rbenv.zsh` for Zsh users.
3. Rehashes shims. From time to time you'll need to rebuild your shim files. Doing this automatically makes sure everything is up to date. You can always run `rbenv rehash` manually.
4. Installs the sh dispatcher. This bit is also optional, but allows rbenv and plugins to change variables in your current shell, making commands like `rbenv shell` possible. The sh dispatcher doesn't do anything invasive like override `cd` or hack your shell prompt, but if for some reason you need `rbenv` to be a real script rather than a shell function, you can safely skip it.

Run `rbenv init -` for yourself to see exactly what happens under the hood.

[AUR (en) - ruby-build (archlinux.org)](https://aur.archlinux.org/packages/ruby-build)

#### *rbenv* Post Install Commands

1. Your shell must be initialized before rbenv will function correctly.
   Run the following, and consider adding it to your ~/.bashrc:

```bash
eval "$(rbenv init -)"
```

2. Add ***rbenv init*** To ***.zshrc***

Edit ***~/.zshrc***. Add this line

```bash
eval "$(rbenv init -)"
```

3. Close the terminal and start up a new one.

4. Install AUR Package ***ruby-build***

   [AUR (en) - ruby-build (archlinux.org)](https://aur.archlinux.org/packages/ruby-build)

   ```bash
   yay -Qs ruby-build <ENTER> # Determine if 'ruby-build' is already installed
   yay -S ruby-build <ENTER> # Install ruby-build
   ```

   Re-boot the system.

   

5. Verify that rbenv is properly set up using this [rbenv-doctor](https://github.com/rbenv/rbenv-installer/blob/main/bin/rbenv-doctor) script:

   ```
   curl -fsSL https://github.com/rbenv/rbenv-installer/raw/main/bin/rbenv-doctor | bash
   ```

   ```
   Checking for `rbenv' in PATH: /usr/local/bin/rbenv
   Checking for rbenv shims in PATH: OK
   Checking `rbenv install' support: /usr/local/bin/rbenv-install (ruby-build 20170523)
   Counting installed Ruby versions: none
     There aren't any Ruby versions installed under `~/.rbenv/versions'.
     You can install Ruby versions like so: rbenv install 2.2.4
   Checking RubyGems settings: OK
   Auditing installed plugins: OK
   ```

6. That's it! Installing rbenv includes ruby-build, so now you're ready to [install some other Ruby versions](https://github.com/rbenv/rbenv#installing-ruby-versions) using `rbenv install`.

#### rehasing shims

Rehashes shims. From time to time you'll need to rebuild your shim files. Doing this automatically makes sure everything is up to date. You can always run `rbenv rehash` manually.

```bash
rbenv rehash
```



#### Installing Ruby versions

See the Ruby Web Site:

[Ruby Programming Language (ruby-lang.org)](https://www.ruby-lang.org/en/)



The `rbenv install` command doesn't ship with rbenv out of the box, but is provided by the [ruby-build](https://github.com/rbenv/ruby-build#readme) project. If you installed it either as part of GitHub checkout process outlined above or via Homebrew, you should be able to:

```bash
# list latest stable versions:
rbenv install -l

# list all local versions:
rbenv install -L

# install a Ruby version (Example):
# rbenv install 2.0.0-p247

# Mike's Version
rbenv install 3.1.0 <ENTER>

```

Set a Ruby version to finish installation and start using commands `rbenv global 2.0.0-p247` or `rbenv local 2.0.0-p247`

```bash
# Mike's Version
rbenv local 3.1.0 <ENTER>
```

Alternatively to the `install` command, you can download and compile Ruby manually as a subdirectory of `~/.rbenv/versions/`. An entry in that directory can also be a symlink to a Ruby version installed elsewhere on the filesystem. rbenv doesn't care; it will simply treat any entry in the `versions/` directory as a separate Ruby version.

#### *ruby*

[Ruby - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Ruby)

#### Installing Ruby gems

Once you've installed some Ruby versions, you'll want to install gems. First, ensure that the target version for your project is the one you want by checking `rbenv version` (see [Command Reference](https://github.com/rbenv/rbenv#command-reference)). Select another version using `rbenv local 2.0.0-p247`, for example. Then, proceed to install gems as you normally would:

```bash
rbenv version <ENTER> # Check the version
gem install bundler # Install gem
```

**You don't need sudo** to install gems. Typically, the Ruby versions will be installed and writeable by your user. No extra privileges are required to install gems.

Check the location where gems are being installed with `gem env`:

```
gem env home
# => ~/.rbenv/versions/<ruby-version>/lib/ruby/gems/...
```

#### Uninstalling Ruby Versions

[GitHub - rbenv/rbenv: Manage your app's Ruby environment](https://github.com/rbenv/rbenv)

As time goes on, Ruby versions you install will accumulate in your `~/.rbenv/versions` directory.

To remove old Ruby versions, simply `rm -rf` the directory of the version you want to remove. You can find the directory of a particular Ruby version with the `rbenv prefix` command, e.g. `rbenv prefix 1.8.7-p357`.

The [ruby-build](https://github.com/rbenv/ruby-build#readme) plugin provides an `rbenv uninstall` command to automate the removal process.

#### Uninstalling *rbenv*

The simplicity of rbenv makes it easy to temporarily disable it, or uninstall from the system.

1. To **disable** rbenv managing your Ruby versions, simply remove the `rbenv init` line from your shell startup configuration. This will remove rbenv shims directory from PATH, and future invocations like `ruby` will execute the system Ruby version, as before rbenv.

   While disabled, `rbenv` will still be accessible on the command line, but your Ruby apps won't be affected by version switching.

2. To completely **uninstall** rbenv, perform step (1) and then remove its root directory. This will **delete all Ruby versions** that were installed under ``rbenv root`/versions/` directory:

   ```
    rm -rf `rbenv root`
   ```

   If you've installed rbenv using a package manager, as a final step perform the rbenv package removal:

   - Archlinux and its derivatives: `sudo pacman -R rbenv`
   - Or `sudo yay -R rbenv`

#### *rbenv* Command Reference

[GitHub - rbenv/rbenv: Manage your app's Ruby environment](https://github.com/rbenv/rbenv)



## *checkhealth*

After installing ***Neovim***, run ***checkhealth***.

```bash
nvim <ENTER> # Launch Neovim

# ---------------------------------------------------
# In the Neovim GUI, issue the 'checkhealth' command.
:checkhealth <ENTER> 
# Go through the errors and correct them!
# --------------------------------------------------

```

### Fix Perl cpan Neovim::Ext

[Neovim::Ext::Plugin::ScriptHost - Neovim Legacy perl Plugin - metacpan.org](https://metacpan.org/pod/Neovim::Ext::Plugin::ScriptHost)

```bash
perl -MCPAN -e shell
install Neovim::Ext
```

For more information visit:

[Installing Perl Modules - www.cpan.org](https://www.cpan.org/modules/INSTALL.html)



