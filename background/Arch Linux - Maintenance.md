



# Arch Linux Maintenance



## Resources



### Videos

[Arch Linux: My Basic Maintenance - YouTube](https://www.youtube.com/watch?v=wwSkFi3h2nI&list=RDCMUCX_WM2O-X96URC5n66G-hvw&start_radio=1&rv=wwSkFi3h2nI&t=38)

[Arch Linux Maintenance | Pacman maintenance - YouTube](https://www.youtube.com/watch?v=3BnHHP7Fmo0)

[Using Pacman on Arch Linux: Everything you need to know - YouTube](https://www.youtube.com/watch?v=-dEuXTMzRKs)

### Articles

[System maintenance - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/System_maintenance)

