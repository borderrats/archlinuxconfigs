[TOC]



# Arch Linux - ZSH



## Resources

[ZSH on the Arch Wiki](https://wiki.archlinux.org/title/zsh)

[Oh My ZSH](https://github.com/ohmyzsh/ohmyzsh)

[Themes](https://github.com/ohmyzsh/ohmyzsh/wiki/Themes)

[Arch Linux Plugin](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins/archlinux)

[Plugins](https://github.com/ohmyzsh/ohmyzsh/tree/master/plugins) 

## Videos

[Arch Linux: Getting Started With ZSH - YouTube](https://www.youtube.com/watch?v=A6xWiqOpulI&t=14s)

[Arch Linux: ZSH With Powerlevel10k - YouTube](https://www.youtube.com/watch?v=dg1tK50UVGI)

