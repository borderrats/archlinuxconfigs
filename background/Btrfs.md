# BTRFS File System
Butter FS File System or Better FS File System


## Resources

[File Systems | Which One is the Best? ZFS, BTRFS, or EXT4 - YouTube](https://www.youtube.com/watch?v=HdEozE2gN9I)

[BTRFS Guide | The Best Desktop File System - YouTube](https://www.youtube.com/watch?v=J2QP4onqJKI)

[Arch Linux Install: January 2021 ISO With BTRFS & Snapshots - YouTube](https://www.youtube.com/watch?v=Xynotc9BKe8)

[Five Years of Btrfs | MarkMcB](https://markmcb.com/2020/01/07/five-years-of-btrfs/)

[Multi-device and RAID1 with btrfs | There and back again (preining.info)](https://www.preining.info/blog/2020/05/multi-device-and-raid1-with-btrfs/)

[How to Set Up Btrfs RAID (linuxhint.com)](https://linuxhint.com/set-up-btrfs-raid/)

[Btrfs - Setup a RAID1 mirror | OpenWritings.net](https://openwritings.net/pg/btrfs/btrfs-setup-raid1-mirror)

[Btrfs - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Btrfs)

[Using Btrfs with Multiple Devices - btrfs Wiki (kernel.org)](https://btrfs.wiki.kernel.org/index.php/Using_Btrfs_with_Multiple_Devices)



## Btrfs Guide 

[BTRFS Guide - Basic Commands, Snapshots, and RAID (christitus.com)](https://www.christitus.com/btrfs-guide)



## Btrfs Web Sites

### Home

[btrfs Wiki (kernel.org)](https://btrfs.wiki.kernel.org/index.php/Main_Page)

### Documentation

[Welcome to BTRFS documentation! — BTRFS 5.14.2 documentation](https://btrfs.readthedocs.io/en/latest/)



## Installing Btrfs

[How to install Arch Linux with BTRFS & Snapper - YouTube](https://www.youtube.com/watch?v=sm_fuBeaOqE)

[Installing Fedora with Timeshift compatible encrypted BTRFS Snapshots - Easy GUI Method! - YouTube](https://www.youtube.com/watch?v=bN8gGoBaZ5M)





## Raid

[Create a btrfs RAID - YouTube](https://www.youtube.com/watch?v=XwH4N4UEQbs)

[Configuring a RAID 1 array with BTRFS on Fedora Linux - YouTube](https://www.youtube.com/watch?v=RzmIoXRkjDs)



## Problems with Btrfs

[BTRFS Disaster | The problem | The solutions - YouTube](https://www.youtube.com/watch?v=Tc8IS9MnDKM)

[BTRFS Guide | The Best Desktop File System - YouTube](https://www.youtube.com/watch?v=J2QP4onqJKI)



