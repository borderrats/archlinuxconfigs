[TOC]



# Arch Linux Plasma (KDE) Desktop



## Resources

### Videos

[[3\] | KDE Customization - YouTube](https://www.youtube.com/watch?v=I-_-B0d_6dI)

[How to install Arch Linux with a Swap file on UEFI and KDE - Part 1: installing the base system - YouTube](https://www.youtube.com/watch?v=rebJqWEiFvg)

[2021.09.01 Arch Linux Plasma Fastest Install - YouTube](https://www.youtube.com/watch?v=vezIymTNSB4)

[How To Install Arch Linux With KDE Plasma (2021) - YouTube](https://www.youtube.com/watch?v=g9k1HVY0IKA)

[Arch Linux Btrfs Kde Plasma Full Install 2021 - The Duck Channel - YouTube](https://www.youtube.com/watch?v=a1vtXCbBHro)

## Arch Linux Wiki

[KDE - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/KDE#Installation)





## The Duck Channel

### KDE Plasma Install Command

```bash
sudo pacman -S plasma kde-applications firefox
```



## EF-Linux Made Simple

### KDE Plasma Install Command

```bash
sudo pacman -S xorg sddm plasma kde-applicaitons packagekit-qt5 firefox libreoffice <ENTER>
```

#### *packagekit*

[PackageKit - What is PackageKit? (www.freedesktop.org)](https://www.freedesktop.org/software/PackageKit/pk-intro.html)

### Enable Display Manager

```
sudo systemctl enable sddm <ENTER> 
```

