[TOC]





# Arch Linux Package Mangers



# pacman

***pacman*** is the package manager for Arch-Linux. It is important that you know and understand ***pacman***.



## Resources

### Videos

[LinuxTV Linux Essentials - The Pacman Command - YouTube](https://www.youtube.com/watch?v=HD7jJEh4ZaM&list=RDCMUCxQKHvKbmSzGMvUrVtJYnUA&index=3)

[DistroTube 2018 - The Pacman Package Manager in Arch Linux - YouTube](https://www.youtube.com/watch?v=-UvZ4BEAXFU)

[Flatpak Tutorial - Setting up Flatpak and installing Packages - YouTube](https://www.youtube.com/watch?v=31WRiI1nk8Q)



### Articles

[LinuxTV - Linux Essentials: The pacman command – LearnLinuxTV](https://www.learnlinux.tv/linux-essentials-the-pacman-command/)

[How to Use Pacman in Arch Linux (linuxhint.com)](https://linuxhint.com/use_pacman_arch_linux/)

[Using Pacman on Arch Linux: Everything you need to know - YouTube](https://www.youtube.com/watch?v=-dEuXTMzRKs)

[List Installed Packages with Pacman on Arch Linux (linuxhint.com)](https://linuxhint.com/list_installed_packages_pacman_arch_linux/)

[How to Remove a Package and Its Dependencies with Pacman on Arch Linux (linuxhint.com)](https://linuxhint.com/remove_package_dependencies_pacman_arch_linux/)

[How to Update the Pacman Databases on Arch Linux (linuxhint.com)](https://linuxhint.com/update_pacman_database_arch_linux/)

[Reinstall All Packages with Pacman on Arch Linux (linuxhint.com)](https://linuxhint.com/reinstall_all_packages_pacman_arch_linux/)

[Pacman command in Arch Linux - GeeksforGeeks](https://www.geeksforgeeks.org/pacman-command-in-arch-linux/)

[Arch Linux - Pacman Mirrorlist Generator](https://archlinux.org/mirrorlist/)



### pacman - Arch Linux

[pacman - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Pacman)

[General recommendations - ArchWiki (archlinux.org) - pacman](https://wiki.archlinux.org/title/General_recommendations#pacman)



## Pacman Mirrors

### Resources

[Setup Pacman Mirrors on Arch Linux (linuxhint.com)](https://linuxhint.com/pacman_mirrors_arch_linux/)

https://linuxhint.com/update_pacman_database_arch_linux/)

[Arch Linux - Pacman Mirrorlist Generator](https://archlinux.org/mirrorlist/)



### Manual Update

1. Go to Arch Linux Mirror List web site: [Arch Linux - Pacman Mirrorlist Generator](https://archlinux.org/mirrorlist/)

2. Select your country or countires from the drop down list (Customized by country mirror list).

3. Modify the Protocol selection criteria. Select ***https, IP4 and Use mirror status***. Then click ***Generate List***. This will present a text listing in the browser. Copy the first dozen or so mirrors. These will be pasted into a configuration file.

4.  Edit the ***mirrorlist*** file and paste the new mirror list data into this file.

   ```bash
   # First create a backup of the mirror list. Just in case.
   cd ./etc/pacman.d
   sudo cp mirrorlist mirrorlist.bak
   
   # Now, empty out the current mirror list
   sudo truncate -s 0 mirrorlist # Empty out the mirror list
   # Note: 0 = truncate to zero bytes
                                 
   -------------------------------------------------------
   # Now, edit the mirrorlist file.
   sudo nano /etc/pacman.d/mirrorlist
   # Replace the existing mirror list data with the
   # new data copied from the Arch Linux Mirror List site.
   ---------------------------------------------------------
   # Paste the new mirror list data into the empty file.
   # Be sure to uncomment several of the servers.
   
   # Example File Content
   ---------------------------------------------------------
   ##
   ## Arch Linux repository mirrorlist
   ## Filtered by mirror score from mirror status page
   ## Generated on 2022-01-13
   ##
   
   ## United States
   Server = https://mirror.dal10.us.leaseweb.net/archlinux/$repo/os/$arch
   ## United States
   Server = https://mirrors.kernel.org/archlinux/$repo/os/$arch
   ## United States
   Server = https://mirrors.sonic.net/archlinux/$repo/os/$arch
   ## United States
   Server = https://iad.mirror.rackspace.com/archlinux/$repo/os/$arch
   ## United States
   Server = https://mirror.pit.teraswitch.com/archlinux/$repo/os/$arch
   ## United States
   Server = https://america.mirror.pkgbuild.com/$repo/os/$arch
   ## United States
   Server = https://mirror.theash.xyz/arch/$repo/os/$arch
   ## United States
   Server = https://mirror.lty.me/archlinux/$repo/os/$arch
   
   
   ```

5. **IMPORTANT** - You MUST resync the mirror list:

   ```bash
   sudo pacman -Syy
   ```

   

### Reflector

#### Install Relector

```bash
sudo pacman -Syy relector <ENTER>
```

#### Configure Mirror List Sort

```bash
sudo relfector -c 'United States' -a 6 --sort rate --save /etc/pacman.d/mirrorlist <ENTER>
```

**-a** - Age

**6** - Only servers that have been updated in the last 6-hours will be included

**--sort rate** - Sort the servers by their speed

**--save** - Save this information to the mirror list

#### Refresh Mirror List

```bash
pacman -Syyy <ENTER>
```



### Pace - GUI for Pacman

[ArcoLinux : 2527 Pace gives you an nice gui of all the important settings of pacman - YouTube](https://www.youtube.com/watch?v=leSHhyQnKZ0)



## Command Resources

[How to Use Pacman in Arch Linux (linuxhint.com)](https://linuxhint.com/use_pacman_arch_linux/)

[Linux Essentials - The Pacman Command - YouTube](https://www.youtube.com/watch?v=HD7jJEh4ZaM&list=RDCMUCxQKHvKbmSzGMvUrVtJYnUA&index=4)



## Refresh The Package Database

The repository cache has information on all the package repositories to which you are subscribed.

### Refresh Package Database Command

**Important:** Refresh the cache before you run ***pacman***.

```bash
sudo sudo pacman -Syy
```

-S = Sync option

1st y = Refresh the package database.

2nd y = Force the local database cache to update, even if it thinks it is already up to date. In other words, refresh the package database no matter what.

## List the Mirror Servers Used to Populate the Package Database

```bash
cat /etc/pacman.d/mirrorlist <ENTER>
```



## Determine Whether an Application is Currently Installed

```bash
which htop <ENTER> 
# This will tell you whether 'htop' application
# is currently installed.
```



## Cleaning the Package Cache

When pacman downloads the packages it stores the packages into the /var/cache/pacman/pkg/ and while uninstalling the package pacman does not delete these files. Pacman uses these files to downgrade the package or install the package. But it can take a lot of space to store these packages. So to delete the stored packages, use the following command:

```bash
sudo pacman -Sc
```



### Delete the Package Cache

To remove all stored packages and cache, use the following command:

```bash
sudp pacman -Scc
```



## Install a Package

### Install Syntax

```bash
sudo pacman -S htop
# Installs the 'htop' application package
```

The snytax for installing a package is:

​	**pacman -S \<PACKAGE NAME\>**

**-S** stands for synchronize.



### Multiple Installation Syntax

```bash
sudo pacman -S htop tmux python-pygame
```



## Installing local packages

By using pacman we can install packages other than the main repository of Arch Linux. Use the following commands to install the packages.

#### For Local Packages

```bash
sudo pacman -U path_to_file.pkg.tar.xz
```



#### For Remote Packages

```bash
sudo pacman -U http://www.example.com/repo/example.pkg.tar.xz
```



## Removing Installed Packages

#### Remove the Package ONLY

```bash
sudo pacman -R <PACKAGE NAME>
```

***-R*** signifies the removal command.

#### Remove Package Along With Dependencies

```bash
sudo pacman -Rs <PACKAGE NAME>
```



## Orphan Dependencies

**Orphan** dependencies are those packages which are no longer required. 

### Identify Orphan Dependencies

```bash
pacman -Qdt <ENTER> # This will list packages
                    # no longer required.
```

**Q** - Query Database

**d** - This option skips dependency cheks

**t** - This option limits results to ***orphan*** packages.

### Delete Orphan Dependencies

This command will remove dependencies which are no longer required. Again, these are called ***orphan*** dependencies.

```bash
sudo pacman -Qdtq |  pacman -Rs # Delete all orphan package
-----------------------------------------------------------
#                     OR
sudo pacman -R <PACKAGE NAME> # Delete packages individually
```

This maintenance should be performed at periodic intervals to prevent unwanted and unecessary growth of the database.



## Upgrade System

This should be done reasonably often in order to upgarde all packages on the system. [EF - Linux Made Simple - YouTube](https://www.youtube.com/channel/UCX_WM2O-X96URC5n66G-hvw) recommends upgrading the system at least every 2-days.  [LearnLinuxTV - YouTube](https://www.youtube.com/channel/UCxQKHvKbmSzGMvUrVtJYnUA) recommends upgrading every 2-weeks.

In Arch Linux, a single command in Pacman not only updates but also syncs and refreshes system packages. The following command upgrades configured packages only and does not update local packages available on the system:

```b
sudo pacman -Syu
```

 **S** tells the pacman to synchronize the local database with the main database. **u** tells the pacman to upgrade the packages and **y** update the local cache on the system. Basically, this command synchronizes the local pacman database with the main repository database and then updates the system.

Lastly, this command updates the package if an update is available. Keep in mind that you must opt for full updates, as partially updated packages are not compatible with Arch Linux.

## Search for a Package

### Search Arch Linux Website for Packages

[Arch Linux - Package Search](https://archlinux.org/packages/)



### Search Server Database for Packages

#### Search Syntax

Pacman is also efficient in searching the packages in your server database based on the name and description of the required package.

```bash
sudo pacman -Ss _string1_ _string2_
```

#### Seach Example

```bash
# pygame is a key word. Not the exact name of the package.
# Key words will return valid results.
pacman -Ss pygame <ENTER> 
```



#### Search for Packages Already Installed on Your System

If you are looking for a package that already exists in your system, then apply the following code. You can add other strings in this code after string2 simultaneously.

```bash
sudo pacman -Qs _string1_ _string2_
```

If you are looking for a package that already exists in your system, then apply the following code. You can add other strings in this code after string2 simultaneously.



### Search Local Package Database

```bash
sudo pacman -F query1>  <query2>
```



## Troubleshooting

Sometimes installing the packages with pacman we face some errors. Following are the mainly occurred errors with pacman:

- **Conflicting file error:** This error occurs due to some conflict packages present in the repository. To solve this error we can manually rename the file or force the overwrite function. We can use the following command to overwrite the function:

```
pacman -S --overwrite glob package
```

- **Invalid package:** This error can occur due to the partial installation of the packages. We can solve this error by deleting .part files in /var/cache/pacman/pkg/.
- **Locking database:** This error can occur when pacman is interrupted while updating the database. To solve this error, delete the file /var/lib/pacman/db.lock and update the database. Use the following command to delete the file:

```
rm /var/lib/pacman/db.lock
```



# AUR - Arch User Repository

Arch User Repository, also known as AUR, is a major part of the Arch Linux ecosystem. It’s a community-driven repository for the Arch Linux system that hosts a number of packages outside the official Arch Linux package database.

Popular AUR projects can eventually get into the official Arch repository! The fact is, a good number of all the new packages added to the Arch Linux official repositories were first AUR packages before becoming official.



## **How AUR works**

AUR is actually a repository of PKGBUILD scripts. It doesn’t necessarily hold the source code of the target package. When you’re using AUR, you’re actually grabbing the PKGBUILD script and building the program for yourself.

PKGBUILD a specialized script for the Arch Linux system that tells the compiler how to build a certain package for the system. The process may include downloading an additional package(s) and source code. The script is also free to have specialized tweaks and fixes for the building process.

So, when you’re getting a package from AUR, you’re actually grabbing the PKGBUILD script. Your system still needs to execute the script and perform the building process to completely install the package.

## **Why AUR?**

The thing is, AUR is one of the pivotal reasons why Arch Linux is such a popular one. Unlike other Linux ecosystems, you’re less susceptible to any permanent security hole in your system. Moreover, AUR packages are easier to install and keep up-to-date, all by yourself.

In the case of AUR, you build the package from source with the help of PKGBUILD. The PKGBUILD script takes care of the entire building process. The maintainer of the package has to create the PKGBUILD, of course. The PKGBUILD system makes the building procedure fluent and hassle-free. Of course, it still holds the potential of installing malicious codes into your system. But instead of adding a complete repo, you just have to worry about the package itself.

Don’t worry; the building procedure is never so simple. It doesn’t take much time, either. Unless, of course, you’re building something heavy like the Linux kernel or Firefox.



## Resources

### Articles

[How to Use AUR with Arch Linux (linuxhint.com)](https://linuxhint.com/aur_arch_linux/)



### AUR Web Sites

[Arch User Repository - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Arch_User_Repository)

[AUR (en) - Packages (archlinux.org)](https://aur.archlinux.org/packages/)



### Videos

[LinuxTV - Linux Essentials - The Arch User Repository (AUR) - YouTube](https://www.youtube.com/watch?v=cBeSJvYkV7I&list=RDCMUCxQKHvKbmSzGMvUrVtJYnUA&index=5)



## **Using AUR**

Enough chitchat about AUR. It’s time to learn how to use AUR.

### **Using AUR Classic Method**

This is the manual and classic way of installing a package from AUR. Everything has to be performed manually, so this path requires more time and patience. Of course, AUR helpers can automate the entire process, but it’s said that one shouldn’t use any AUR helper if one isn’t able to use AUR manually.

#### **Installing a package, Classic Method**

Installing a package from AUR is quite different than installing a package with pacman. At first, make sure that your system includes all the necessary tools to perform the building process.

```bash
sudo pacman -Syu

sudo pacman -S git base-devel
```

Once your system is ready, grab the AUR package with Git.

```bash
git clone https://aur.archlinux.org/<pkgName>.git
```

Change the active directory.

```bash
cd <pkgName>/
```

Start the building process.

```bash
makepkg
```

Once the building process is complete, there will be a TAR.XZ file in the directory. Install it using ***pacman***.

```bash
sudo pacman -U <package_name>.tar.xz
```

​                                 **OR**

*Note: The installation process can also be done with the previous step.*

```bash
makepkg -sri
```



### Sample Installation

1. Pull up the AUR Packages web page to search for your package:

​		[AUR (en) - Packages (archlinux.org)](https://aur.archlinux.org/packages/)

​		[AUR (en) - Search Criteria: google chrome (archlinux.org)](https://aur.archlinux.org/packages/?O=0&K=google+chrome)

2. Check the comments. Be cautious. Look for issues.

3. Right Click ***Download snapshot*** and copy the link.

4. Download the package with ***wget***

   ```bash
   wget https://aur.archlinux.org/cgit/aur.git/snapshot/google-chrome.tar.gz
   ```

5. Unzip the tar:

   ```bash
   tar -xvf google-chrome.tar.gz # Unzip tar to home directory
   ```

6. Go to the newly created google chrome directory:

   ```bash
   cd google-chrome
   ```

7. Review the ***PKGBUILD*** and understand what it is doing.

   ```bash
   nano PKGBUILD
   ```

   Check the script. Focus on the ***rm*** commands.

8. Make sure the base-devel package group is installed:

   ```bash
   sudo pacman -S base-devel <ENTER>
   ```

9. Run the ***makepkg***. This package is provided by ***pacman***.

    https://www.vultr.com/docs/using-makepkg-on-arch-linux 

   There are many options for `makepkg`, including these commonly used ones:

   - ```
     -s, --syncdeps
     ```

     : Automatically install missing dependencies required for building and using the package.

     - Note if the `PKGBUILD` has dependencies on AUR packages, you need to install those first, or have them available in a local repository of your own so it can find them.

   - ```
     -r, --rmdeps
     ```

     : If it builds successfully, automatically uninstall dependencies that are no longer needed, including the following:

     - Ones required for building but not using the package.
     - If used without the `--install` option, ones required for using the package.

   - `-i, --install` : If it builds successfully, automatically install/upgrade the package.

   - `-c, --clean` : Automatically delete temporary build files, usually only needed when debugging a build that has gone wrong.

   To automatically install/upgrade the package, leave dependencies installed for using the package, and remove dependencies only needed during building, run:

   ```
   makepkg -sri
   ```

   You must be in the folder which contains the package build when you run this command:

   ```bash
   makepkg -s
   
   sudo pacman -U <NAME OF PACKAGE>
   ```

   

### **Updating a package, Classic Method**

The installed AUR packages won’t update them automatically. You have to manually update the package by following the previous steps.

Grab the latest PKGBUILD from AUR.

```bash
git clone https://aur.archlinux.org/<pkgName>.git
```

*Note: If you didn’t delete the previously created directory, navigate to it and run the following command. Git will upgrade the directory with the latest file(s) and changes.*

```bash
git pull
```

Now, re-run the building and installing process.

```bash
makepkg -s <ENTER>
sudo pacman -U <pkgName>.tar.zst <ENTER>
```

​                                **OR**

```bash
makepkg -sri
sudo pacman -U <pkgName>.tar.zst <ENTER>
```



## **AUR helpers - *yay* and *pacaur* **

To make life easier, here come the AUR helpers. AUR helpers can automate a large portion of the processes, saving time and trouble.

According to the official Arch Linux AUR helpers wiki, there are numerous AUR helpers out there. While some of them are officially trusted, all of them are capable of performing all the basic jobs efficiently. In this case, we’ll only include a handful AUR helpers that are both trusted and actively maintained.

- [auracle-git](https://aur.archlinux.org/packages/auracle-git/)
- [aurutils](https://aur.archlinux.org/packages/aurutils/)
- [trizen](https://aur.archlinux.org/packages/trizen/)
- [yay](https://aur.archlinux.org/packages/yay/)
- [pacaur](https://aur.archlinux.org/packages/pacaur/)

In this tutorial, we’ll only focus on yay as it’s one of the most popular ones on AUR. It borrows its designs from a number of other AUR helpers like Yaourt (discontinued), Pacaur and Apacman.

### **Yay – Yet another Yogurt**

Installing yay requires the knowledge of installing an AUR package manually. Let’s get yay ready.

At first, grab ***yay*** from AUR.

```bash
sudo pacman -S git # Make sure git is installed

git clone https://aur.archlinux.org/yay.git

cd yay <ENTER> # Go into newly created 'yay' directory

nano PKGBUILD <ENTER> # Edit the PKGBUILD file just to be safe
# Check it out.

makepkg -s # Build the package

sudo pacman -U yay.10.1.2-2-x86_64.pkg.tar.zst

```



### Using *yay*

[How to Use AUR with Arch Linux (linuxhint.com)](https://linuxhint.com/aur_arch_linux/)

```bash
yay linode-cli
# This will list multiple packages containg keyword "linode-cli"
# The found packages will be numbered. 
# To install a specific package, enter the number and press ENTER.
```



### Update All Packages with *yay*

***yay*** can be used to update **ALL** packages on your system. This includes both Arch Linux packages and Arch Linux User Repoistory packages. This global update comand is:

```bash
yay -Syu # Updates all packages on your system
```

