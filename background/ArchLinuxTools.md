[TOC]

------



# Arch Linux

## GitLab

[Ermanno Ferrari · GitLab](https://gitlab.com/eflinux)



## Packages

### Pacman

[Arch Linux - Package Search](https://archlinux.org/packages/)

### Arch User Repository

[AUR (en) - Home (archlinux.org)](https://aur.archlinux.org/)



## Websites

### Home

[Arch Linux](https://archlinux.org/)

[Table of contents - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Table_of_contents)

### Downloads

[Arch Linux - Downloads](https://archlinux.org/download/)

### Wiki

[ArchWiki (archlinux.org)](https://wiki.archlinux.org/)

### Installation Guide

[Installation guide - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Installation_guide)



# Arch Linux Tools

## AUR - Arch Linux User Repository

[AUR (en) - Home (archlinux.org)](https://aur.archlinux.org/)

[LinuxTV - Linux Essentials - The Arch User Repository (AUR) - YouTube](https://www.youtube.com/watch?v=cBeSJvYkV7I&list=RDCMUCxQKHvKbmSzGMvUrVtJYnUA&index=5)

[How to Use the AUR with Arch Linux (linuxhint.com)](https://linuxhint.com/aur_arch_linux-2/)



## Backup Tools - Arch Linux

[Best Backup and Restore Tools for Arch Linux (linuxhint.com)](https://linuxhint.com/best_backup_restore_arch_linux/)'

[How to Backup an Arch Linux System (linuxhint.com)](https://linuxhint.com/backup_arch_linux_system/)



## cfdisk

[How to Create, Resize and Delete Linux Partitions With Cfdisk (makeuseof.com)](https://www.makeuseof.com/how-to-create-resize-and-delete-linux-partitions-with-cfdisk/)

[cfdisk command in Linux with examples - GeeksforGeeks](https://www.geeksforgeeks.org/cfdisk-command-in-linux-with-examples/)

[Linux cfdisk command help and examples (computerhope.com)](https://www.computerhope.com/unix/cfdisk.htm)



## Configuration Scripts

[Derek Taylor · GitLab-Dotfiles](https://gitlab.com/dwt1)



## Desktops

[Choosing a Desktop Environment (Linux for Noobs 3) - YouTube](https://www.youtube.com/watch?v=IiOt4waFqhQ)



## Dialog 

A tool to display dialog boxes from shell scripts.

[Arch Linux - dialog 1:1.3_20211214-1 (x86_64)](https://archlinux.org/packages/core/x86_64/dialog/)



## Disk Commands

### lsblk

[lsblk Command in Linux with Examples - GeeksforGeeks](https://www.geeksforgeeks.org/lsblk-command-in-linux-with-examples/)

```bash
lsblk	# List Block Command. 
# List devices connected to computer. Does NOT include RAM
```



### gdisk

Used with UEFI systems.

https://www.bcdowd.com/gdisk-commands

[gdisk linux command man page (commandlinux.com)](https://www.commandlinux.com/man-page/man8/gdisk.8.html)

[gdisk Command: How to Create GUID Partitions (GPT) in Linux - Edumotivation](https://edumotivation.com/how-to-create-guid-partitions-in-linux/)



## Dual Boot Windows and Arch Linux

[How to Dual Boot Arch Linux Windows 10 (linuxhint.com)](https://linuxhint.com/dual-boot-arch-linux-windows-10/)

gpg - Verify Signature

***gpg*** is already installed on Mike's Windows 10.

```bash
$ gpg --keyserver-options auto-key-retrieve --verify archlinux-version-x86_64.iso.sig
```



## Editors

### Visual Studio Code

[Running Visual Studio Code on Linux](https://code.visualstudio.com/docs/setup/linux)

[AUR (en) - visual-studio-code-bin (archlinux.org)](https://aur.archlinux.org/packages/visual-studio-code-bin)

[Install Visual Studio Code on Arch Linux - YouTube](https://www.youtube.com/watch?v=H14-kzMWq98&t=1s)

[Installing Microsoft VS Code on Arch Linux - LinuxForDevices](https://www.linuxfordevices.com/tutorials/linux/vs-code-on-arch)



## Internet

```bash
ip -c a  # List internet interfaces

```

EF installed the following network utilities. See 24:29 at:

[Arch Linux Monthly Install: 12.2021 - YouTube](https://www.youtube.com/watch?v=gyiHrsr4IPA)

```bash
pacman -S grub efibootmgr networkmanager network-manager-applet dialog base-devel linux-headers xdg-utils xdg-user-dirs cups pulseaudio alsa-utils pavucontrol terminus-font
```

**alsa-utils** - Advanced Linux Sound Architecture - Utilities

**cups** - The CUPS Printing System - daemon package.

**dialog** - A tool to display dialog boxes from shell scripts.

**network-manager-applet**  Applet for managing network connections.

**pavucontrol** - PulseAudio Volume Control.

**xdg-utils** - Command line tools that assist applications with a variety of desktop integration tasks.

**xdg-user-dirs** -  Manage user directories like ~/Desktop and ~/Music



## pacman

***pacman*** is the package manager for Arch-Linux. It is important that you know and understand ***pacman***.

[General recommendations - ArchWiki (archlinux.org) - pacman](https://wiki.archlinux.org/title/General_recommendations#pacman)

[pacman Documentation - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Pacman)

[Linux Essentials - The Pacman Command - YouTube](https://www.youtube.com/watch?v=HD7jJEh4ZaM&list=RDCMUCxQKHvKbmSzGMvUrVtJYnUA&index=3)

[DistroTube 2018 - The Pacman Package Manager in Arch Linux - YouTube](https://www.youtube.com/watch?v=-UvZ4BEAXFU)

[How to Use Pacman in Arch Linux (linuxhint.com)](https://linuxhint.com/use_pacman_arch_linux/)

[Using Pacman on Arch Linux: Everything you need to know - YouTube](https://www.youtube.com/watch?v=-dEuXTMzRKs)

[List Installed Packages with Pacman on Arch Linux (linuxhint.com)](https://linuxhint.com/list_installed_packages_pacman_arch_linux/)

[How to Remove a Package and Its Dependencies with Pacman on Arch Linux (linuxhint.com)](https://linuxhint.com/remove_package_dependencies_pacman_arch_linux/)

[How to Update the Pacman Databases on Arch Linux (linuxhint.com)](https://linuxhint.com/update_pacman_database_arch_linux/)

[Setup Pacman Mirrors on Arch Linux (linuxhint.com)](https://linuxhint.com/pacman_mirrors_arch_linux/)

[Reinstall All Packages with Pacman on Arch Linux (linuxhint.com)](https://linuxhint.com/reinstall_all_packages_pacman_arch_linux/)

[How to Update the Pacman Databases on Arch Linux (linuxhint.com)](https://linuxhint.com/update_pacman_database_arch_linux/)

### Pace - GUI for Pacman

[ArcoLinux : 2527 Pace gives you an nice gui of all the important settings of pacman - YouTube](https://www.youtube.com/watch?v=leSHhyQnKZ0)



## Printing

### cups Package

The CUPS Printing System - daemon package.

[CUPS.org](https://www.cups.org/)

[OpenPrinting CUPS](https://openprinting.github.io/cups/)

[Arch Linux Package - cups 1:2.4.0-4 (x86_64)](https://archlinux.org/packages/extra/x86_64/cups/)

### Brother Printer Installation

[How to install a Brother printer on Linux (xmodulo.com)](https://www.xmodulo.com/install-brother-printer-linux.html)

[To install the Brother printer driver easily using a tool. (Linux) | Brother](https://support.brother.com/g/b/faqend.aspx?c=us&lang=en&prod=dcpl2550dw_us&faqid=faq00100556_000)

[Installing the LPR driver and CUPS wrapper driver (Linux®) | Brother](https://support.brother.com/g/b/faqend.aspx?c=us_ot&lang=en&prod=lptd2120neus&faqid=faqp00100414_000)



## LVM

[Logical Volume Manager](D:\Programming\Linux\LVM\LogicalVolumeManager.md)



## Set Font Size

In initial Arch Linux Media start-up you can set font size as follows:

```bash
setfont ter-132n
```

### SSH

[Install and Configure SSH Server on Arch Linux (linuxhint.com)](https://linuxhint.com/install_ssh_server_on_arch_linux/)



## Update Procedures

[How to Update Arch Linux (linuxhint.com)](https://linuxhint.com/update-arch-linux/)

[How to Make Arch Linux Stable and What NOT to Do! - YouTube](https://www.youtube.com/watch?v=xhVS1HKwGWw)

### What NOT to Do

[How to Make Arch Linux Stable and What NOT to Do! - YouTube](https://www.youtube.com/watch?v=xhVS1HKwGWw)

#### Don't Update Every Day

- Backup once per week

#### Proper backup

Use ***btrfs*** timeshare and snapshot

#### Pin Linux

```bash
sudo pacman -Syu # List packages for upgrade
```

[How to Make Arch Linux Stable and What NOT to Do! - YouTube](https://www.youtube.com/watch?v=xhVS1HKwGWw)

Find a good Linux version and pin it.  Keep the "lts linux" version. Pin the "linux" bleeding edge version.

```bash
sudo nano /etc/pacman.conf
## Go to 
#Ignore Pkg = 
## Uncomment this line and add linux
Ignore Pkg = linux
```



```bash
uname -sr ## Display linux versions
```



#### Do NOT install Out-of-Date Packages



## VMWare

[VMware/Install Arch Linux as a guest - ArchWiki](https://wiki.archlinux.org/title/VMware/Install_Arch_Linux_as_a_guest#Installation_(from_guest))

[Install Arch Linux in VMware Workstation - Linux Tutorials - Learn Linux Configuration](https://linuxconfig.org/install-arch-linux-in-vmware-workstation)

[Install Arch Linux on VMware Workstation 2021 - Step by Step - YouTube](https://www.youtube.com/watch?v=l8YPS2itHfU)

[Arch Install - YouTube](https://www.youtube.com/watch?v=hAhLgnwO5NM)

### Wallpaper

[How to Customize the Wallpaper in Arch Linux (linuxhint.com)](https://linuxhint.com/customize_wallpaper_arch_linux/)



## Window Managers

[Window Managers](D:\Programming\Linux\WindowsManagers\LinuxWindowsManagers.md)