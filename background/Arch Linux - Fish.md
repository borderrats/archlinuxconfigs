[TOC]





# Arch Linux - Fish

Friendly Interactive Shell

[Arch Linux: Getting Started With Fish - YouTube](https://www.youtube.com/watch?v=YC9gBFSI-70&list=RDCMUCX_WM2O-X96URC5n66G-hvw&start_radio=1&rv=YC9gBFSI-70&t=72)



## Resources



### Videos

[Arch Linux: Getting Started With Fish - YouTube](https://www.youtube.com/watch?v=YC9gBFSI-70&list=RDCMUCX_WM2O-X96URC5n66G-hvw&start_radio=1&rv=YC9gBFSI-70&t=72)

[Arch Linux: Fish Plugins & Themes - YouTube](https://www.youtube.com/watch?v=ZjArkI5xwJI&list=RDCMUCX_WM2O-X96URC5n66G-hvw&index=24)



## Installation

### Arch Linux *pacman* Installation

```bash
sudo pacman -S fish pkgfile ttf-dejavu powerline-fonts inetutils

# ttf-dejavu a font
```



### Configure *bash.rc* File

#### Edit *bash.rc* File

```bash
nano .bashrc
```

#### Add Line to *bash.rc*

```bash
# Add this line as last line in bash.rc file
exec fish
```

#### Re-Initialize *bash.rc*

Either reboot the machine or source the *bash.rc* file like this:

```bash
source .bash.rc<ENTER>
```



### Features

- *fish* adds auto completion
- Generating auto completions for the ***man*** pages.

### Examine *fish* Directory

#### List Files in *fish* Directory

```bash
cd /usr/share/fish<ENTER> # Go to fish directory
ls<ENTER> #list the files and directories
```

#### Identify the *fish* Configuration File

```bash
config.fish # The fish configuration file
```

### Create and Configure *config.fish* in Home Directory

```bash
nano .config/fish/config.fish
```

### Add *neofetch* to *config.fish*

```bash
# Add this item as first line to new file config.fish opened above

neofetch # Save and exit
```

### Install *neofetch*

```bash
packman -S neofetch<ENTER>
```

### Test Configuration

Restart the terminal.



### Remove *neofetch* from config.fish

```bash
nano .config/fish/config.fish # Edit config file
# Delete the line containing neofetch. Then save & exit.
# I know this sounds crazy. All will be explained later.
```



### Perform Configuration from a Browser

#### Start Browser

Issue the following command from the terminal:

```bash
fish_config<ENTER> # Starts configuration tool in default browser
```

#### Configure Colors, Prompts, Themes

7:11 - [Arch Linux: Getting Started With Fish - YouTube](https://www.youtube.com/watch?v=YC9gBFSI-70&list=RDCMUCX_WM2O-X96URC5n66G-hvw&start_radio=1&rv=YC9gBFSI-70&t=72)

```bash
q<ENTER> # q = quit
```

#### Configure alias Short-Cut Key Bindings

```bash
# From the Terminal Prompt
# Example 'alias'
alias -s cfh "cd /home/mike/.config/fish"
# This alias will take you to the fish configuration directory
```

You can always delete an alias by going into the ***fish/functions*** directory and deleting the file containing the alias. In this example that means deleting the file ***cfh.fish***.

## Add Autocompletions for *man* Pages

```bash
fish_update_completions<ENTER>
```



# Fish Plugins & Themes

[Arch Linux: Fish Plugins & Themes - YouTube](https://www.youtube.com/watch?v=ZjArkI5xwJI&list=RDCMUCX_WM2O-X96URC5n66G-hvw&index=24)

## Install from Github

[GitHub - oh-my-fish/oh-my-fish: The Fish Shell Framework](https://github.com/oh-my-fish/oh-my-fish)

### Clone *oh-my-fish* Github Repository

```bash
# Should probably be in the home directory when doing this.

git clone https://github.com/oh-my-fish/oh-my-fish<ENTER>
```

### Run the *oh-my-fish* Installation Script

```bash
cd oh-my-fish/ # Make sure you are in the newly created on-my-fish
               # directory off you 'home' directory
               
# Execute the installation script
bin/install --offline<ENTER>
```



## Explore the *oh-my-fish* Framework

- The frame work allows us to install plugins and themes.
- [Oh My Fish! · GitHub - Theme and Plugin Documentation](https://github.com/oh-my-fish?page=1)



### List Exiting oh-my-fish Items

```bash
omf list<ENTER>
```

### Install Themes

#### List Available *oh-my-fish* Themes

```bash
omf theme<ENTER> # Displays the available themes
```

#### Reivew Graphical Presentation of *oh-my-fish* Themes

[oh-my-fish/Themes.md at master · oh-my-fish/oh-my-fish · GitHub](https://github.com/oh-my-fish/oh-my-fish/blob/master/docs/Themes.md)



### Install a Few Themes

```bash
# Examples
omf install batman<ENTER> # Them is applied immediately

omf install agnoster<ENTER>

omf install spacefish<ENTER>


```

### *bobthefish* Theme

```bash
omf install bobthefish<ENTER> # This them is configurable
```

#### Configure *bobthefish* Theme

##### Example

```bash
set -g theme_display_user yes<ENTER> # Display user name

set -g theme_color_scheme solrized<ENTER>

```

##### Theme Documentation

[GitHub - oh-my-fish/theme-bobthefish: A Powerline-style, Git-aware fish theme optimized for awesome.](https://github.com/oh-my-fish/theme-bobthefish)



### Install plugins

#### List Plugins

```bash
omf list<ENTER>

omf install<TAB><TAB> #Lists both themes and plugins 
```

```bash
<TAB> # Hit 'TAB' one more time to display available plugins
```

#### Example Plugin Installation: Arch Linux

```bash
omf install archlinux
```

Once installed, you can use the ***archlinux*** plugin to perform various tasks associated with Arch Linux. For example, to install the browser ***firefox***, you can use this shortcut for the ***pacman*** install command:

```bash
pacin fireforx<ENTER> # Calls 'pacman' to install firefox.

pacre firefox<ENTER> # Removes the package but retains
                     # dependencies and configurations

pacrem firefox<ENTER> # Removes the package plus all
                      # the dependencies and configurations.
                      
```

#### Documentation for ***archlinux*** Plugin

[plugin-archlinux/functions at master · oh-my-fish/plugin-archlinux · GitHub](https://github.com/oh-my-fish/plugin-archlinux/tree/master/functions)



## Activate *neofetch*

### Edit Personal *fish* Configuration File

```bash
nano .config/fish/config.fish
```

### Add *neofetch* on the First Line

```bash
neofetch	# Save and Exit the file
```



## Make Final Selection of *oh-my-fish* Theme

```bash
omf theme batman<ENTER>
```

## Remove Unused Themes

```bash
# Example of unused theme needing removal
omf remove agnoster<ENTER>
```

## Update *oh-my-fish* Framework

```bash
omf update<ENTER> #Updates the oh-my-fish framework
```

