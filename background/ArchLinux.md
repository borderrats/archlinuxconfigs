[TOC]



# Arch Linux



## Packages

### Pacman

[Arch Linux - Package Search](https://archlinux.org/packages/)

## Websites

### Arch Linux Official Sites

#### Arch Home

[Arch Linux](https://archlinux.org/)

[Table of contents - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Table_of_contents)

#### Arch Downloads

[Arch Linux - Downloads](https://archlinux.org/download/)

#### Arch Wiki

[ArchWiki (archlinux.org)](https://wiki.archlinux.org/)

#### Arch Installation Guide

[Installation guide - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Installation_guide)

#### Arch User Repository

[AUR (en) - Home (archlinux.org)](https://aur.archlinux.org/)

### EFI

#### Website

[Home - eflinux.com](https://www.eflinux.com/)

#### Repository

[Ermanno Ferrari · GitLab](https://gitlab.com/eflinux)



## Resources

### Videos

[Resetting Arch Linux - YouTube](https://www.youtube.com/watch?v=2vbrFZiq2Hc)

[Arch Linux Isn't *THAT* Hard. - YouTube](https://www.youtube.com/watch?v=PsNLZwuUJLU)

[Arch Linux Installation Guide 2020 - YouTube](https://www.youtube.com/watch?v=PQgyW10xD8s)

[Should I use Arch? 5 Reasons to use Arch Linux and 3 Reasons not to use Arch Linux  2019-2020  - YouTube](https://www.youtube.com/watch?v=CaibJjQaELU)

[How to Make Arch Linux Stable and What NOT to Do! - YouTube](https://www.youtube.com/watch?v=xhVS1HKwGWw)

[Arch Linux Anarchy Installer Is Almost Perfect - YouTube](https://www.youtube.com/watch?v=2UCRSCQ6QiI)

[DistroTube-Arch Linux Installation Guide 2020 - YouTube](https://www.youtube.com/watch?v=PQgyW10xD8s&list=RDCMUCVls1GmFKf6WlTraIb_IaJg&index=1)

[DistroTube-Installing Xorg And A Window Manager In Arch Linux - YouTube](https://www.youtube.com/watch?v=pouX5VvX0_Q&list=RDCMUCVls1GmFKf6WlTraIb_IaJg&index=2)

[LinuxTV - Arch Linux: Full Installation Guide - A complete tutorial/walkthrough in one video! - YouTube](https://www.youtube.com/watch?v=DPLnBPM4DhI)

[LinuxTV - Linux Essentials - The Arch User Repository (AUR) - YouTube](https://www.youtube.com/watch?v=cBeSJvYkV7I&list=RDCMUCxQKHvKbmSzGMvUrVtJYnUA&index=5)

[Linux Essentials - The Pacman Command - YouTube](https://www.youtube.com/watch?v=HD7jJEh4ZaM&list=RDCMUCxQKHvKbmSzGMvUrVtJYnUA&index=3)



### GitLab

[Ermanno Ferrari · GitLab](https://gitlab.com/eflinux)



### LinuxHint

[Search Results for “arch linux” (linuxhint.com)](https://linuxhint.com/?s=arch+linux)



### AUR

[How to Use the AUR with Arch Linux (linuxhint.com)](https://linuxhint.com/aur_arch_linux-2/)



## Tools

### Backup Tools - Arch Linux

[Best Backup and Restore Tools for Arch Linux (linuxhint.com)](https://linuxhint.com/best_backup_restore_arch_linux/)'

[How to Backup an Arch Linux System (linuxhint.com)](https://linuxhint.com/backup_arch_linux_system/)



### Desktops

[Choosing a Desktop Environment (Linux for Noobs 3) - YouTube](https://www.youtube.com/watch?v=IiOt4waFqhQ)



### Editors

[How to Install Atom On Arch Linux (linuxhint.com)](https://linuxhint.com/install-atom-arch-linux/)



### Packman

[How to Use Pacman in Arch Linux (linuxhint.com)](https://linuxhint.com/use_pacman_arch_linux/)

[List Installed Packages with Pacman on Arch Linux (linuxhint.com)](https://linuxhint.com/list_installed_packages_pacman_arch_linux/)

[How to Remove a Package and Its Dependencies with Pacman on Arch Linux (linuxhint.com)](https://linuxhint.com/remove_package_dependencies_pacman_arch_linux/)

[How to Update the Pacman Databases on Arch Linux (linuxhint.com)](https://linuxhint.com/update_pacman_database_arch_linux/)

[Setup Pacman Mirrors on Arch Linux (linuxhint.com)](https://linuxhint.com/pacman_mirrors_arch_linux/)

[Reinstall All Packages with Pacman on Arch Linux (linuxhint.com)](https://linuxhint.com/reinstall_all_packages_pacman_arch_linux/)

[How to Update the Pacman Databases on Arch Linux (linuxhint.com)](https://linuxhint.com/update_pacman_database_arch_linux/)



### SSH

[Install and Configure SSH Server on Arch Linux (linuxhint.com)](https://linuxhint.com/install_ssh_server_on_arch_linux/)



### VMWare Installation

[Arch Install - YouTube](https://www.youtube.com/watch?v=hAhLgnwO5NM)



### Wallpaper

[How to Customize the Wallpaper in Arch Linux (linuxhint.com)](https://linuxhint.com/customize_wallpaper_arch_linux/)



### XMonad

[XMonad on Arch Linux - Custom Install a STUNNING & Minimal WM/Desktop - Linux Rice in 2021! - YouTube](https://www.youtube.com/watch?v=LBNEW4ZKvEc)

[GitHub - Axarva/dotfiles-2.0: XMonad™️. Widgets go brr.](https://github.com/Axarva/dotfiles-2.0)

[Getting Started With Xmonad - YouTube](https://www.youtube.com/watch?v=3noK4GTmyMw)



## Dual Boot Windows and Arch Linux

[How to Dual Boot Arch Linux Windows 10 (linuxhint.com)](https://linuxhint.com/dual-boot-arch-linux-windows-10/)



## Update Procedures

[How to Update Arch Linux (linuxhint.com)](https://linuxhint.com/update-arch-linux/)



## Installation Notes

### gpg

***gpg*** is already installed on Mike's Windows 10.



### Partition Disk

```bash
fdisk -l	## List all available drives. In VM's, ignore the 2nd loop back device.
			## The real drive should be the top one.
fdisk /dev/sda	## Will ask for more commands.  Type 'm' for help.
##
```



## Post Installation Procedures

[How to Customize Arch Linux After Installing It (linuxhint.com)](https://linuxhint.com/customize-arch-linux/)

### Install GCC

[How to Install GCC for Arch Linux (linuxhint.com)](https://linuxhint.com/install-gcc-arch-linux/)





## What NOT to Do

[How to Make Arch Linux Stable and What NOT to Do! - YouTube](https://www.youtube.com/watch?v=xhVS1HKwGWw)

### Don't Update Every Day

- Backup once per week

### Proper backup

Use ***btrfs*** timeshare and snapshot

### Pin Linux

```bash
sudo pacman -Syu # List packages for upgrade
```

[How to Make Arch Linux Stable and What NOT to Do! - YouTube](https://www.youtube.com/watch?v=xhVS1HKwGWw)

Find a good Linux version and pin it.  Keep the "lts linux" version. Pin the "linux" bleeding edge version.

```bash
sudo nano /etc/pacman.conf
## Go to 
#Ignore Pkg = 
## Uncomment this line and add linux
Ignore Pkg = linux
```



```bash
uname -sr ## Display linux versions
```



## Do NOT install Out-of-Date Packages



## Maintenance In Arch Linux

[System maintenance - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/System_maintenance)

[Arch Linux Maintenance | Pacman maintenance - YouTube](https://www.youtube.com/watch?v=3BnHHP7Fmo0)

[Using Pacman on Arch Linux: Everything you need to know - YouTube](https://www.youtube.com/watch?v=-dEuXTMzRKs)

[Arch Linux: My Basic Maintenance - YouTube](https://www.youtube.com/watch?v=wwSkFi3h2nI)



## Backup

[Synchronization and backup programs - ArchWiki (archlinux.org)](https://wiki.archlinux.org/title/Synchronization_and_backup_programs)



# Arco Linux

## Resources

[ArcoLinux | ArcoLinux](https://arcolinux.com/)

[Arco Linux Xtended Has All The Window Managers - YouTube](https://www.youtube.com/watch?v=J5mkCQ5YnH4)

[ArcoLinux OS - Fun & Beautiful Arch Distro | My New Favorite Distro? - YouTube](https://www.youtube.com/watch?v=QC9KauUT4lI)



## Pace - GUI for Pacman

[ArcoLinux : 2527 Pace gives you an nice gui of all the important settings of pacman - YouTube](https://www.youtube.com/watch?v=leSHhyQnKZ0)



# Endeavor OS

## Videos

[EndeavourOS - BEST Arch Distro (New Release!) - YouTube](https://www.youtube.com/watch?v=gGeA7QQIfp4)

[EndeavourOS Atlantis | One Of The Best Arch Based Distros - YouTube](https://www.youtube.com/watch?v=q3LuPQ5-dXY)

[Endeavour OS 21.4 Atlantis: Arch Linux For Normal People - YouTube](https://www.youtube.com/watch?v=y7liEAjBqlc)

[7 Things After Installing EndeavourOS (MUST DO!) - YouTube](https://www.youtube.com/watch?v=StHVU-Zvacs)



## Website

[EndeavourOS – A terminal-centric distro with a vibrant and friendly community at its core](https://endeavouros.com/)





# Manjaro Linux

[Arch made EASY - Manjaro Linux vs EndeavourOS - YouTube](https://www.youtube.com/watch?v=jv9VSh5zIGw)

